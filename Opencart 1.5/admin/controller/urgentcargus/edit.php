<?php
class ControllerUrgentCargusEdit extends Controller {
    private $error = array();

    public function index(){
        $this->language->load('urgentcargus/edit');

		$this->document->setTitle($this->language->get('heading_title'));

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];
            $this->session->data['success'] = '';
        } else {
            $this->data['success'] = '';
        }
        if (isset($this->session->data['error'])) {
            $this->data['error'] = $this->session->data['error'];
            $this->session->data['error'] = '';
        } else {
            $this->data['error'] = '';
        }
        if (isset($this->session->data['error_warning'])) {
            $this->data['error_warning'] = $this->session->data['error_warning'];
            $this->session->data['error_warning'] = '';
        } else {
            $this->data['error_warning'] = '';
        }

        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['text_pending'] = $this->language->get('text_pending');
        $this->data['text_shipping'] = $this->language->get('text_shipping');
        $this->data['text_choose_pickup'] = $this->language->get('text_choose_pickup');
        $this->data['text_no_data'] = $this->language->get('text_no_data');
        $this->data['text_sender'] = $this->language->get('text_sender');
        $this->data['text_recipient'] = $this->language->get('text_recipient');
        $this->data['text_details'] = $this->language->get('text_details');
        $this->data['text_sender'] = $this->language->get('text_sender');
        $this->data['text_recipient'] = $this->language->get('text_recipient');
        $this->data['text_no'] = $this->language->get('text_no');
        $this->data['text_yes'] = $this->language->get('text_yes');
        $this->data['text_urgent_destinatie'] = $this->language->get('text_urgent_destinatie');
        $this->data['text_urgent_franciza'] = $this->language->get('text_urgent_franciza');
        $this->data['entry_pickup'] = $this->language->get('entry_pickup');
        $this->data['entry_county'] = $this->language->get('entry_county');
        $this->data['entry_locality'] = $this->language->get('entry_locality');
        $this->data['entry_name'] = $this->language->get('entry_name');
        $this->data['entry_address'] = $this->language->get('entry_address');
        $this->data['entry_contact'] = $this->language->get('entry_contact');
        $this->data['entry_phone'] = $this->language->get('entry_phone');
        $this->data['entry_email'] = $this->language->get('entry_email');
        $this->data['entry_parcels'] = $this->language->get('entry_parcels');
        $this->data['entry_envelopes'] = $this->language->get('entry_envelopes');
        $this->data['entry_weight'] = $this->language->get('entry_weight');
        $this->data['entry_value'] = $this->language->get('entry_value');
        $this->data['entry_cash_repayment'] = $this->language->get('entry_cash_repayment');
        $this->data['entry_bank_repayment'] = $this->language->get('entry_bank_repayment');
        $this->data['entry_other_repayment'] = $this->language->get('entry_other_repayment');
        $this->data['entry_payer'] = $this->language->get('entry_payer');
        $this->data['entry_saturday_delivery'] = $this->language->get('entry_saturday_delivery');
        $this->data['entry_morning_delivery'] = $this->language->get('entry_morning_delivery');
        $this->data['entry_openpackage'] = $this->language->get('entry_openpackage');
        $this->data['entry_observations'] = $this->language->get('entry_observations');
        $this->data['entry_contents'] = $this->language->get('entry_contents');
        $this->data['entry_shipping_method'] = $this->language->get('entry_shipping_method');
        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');
        $this->data['cancel'] = $this->url->link('urgentcargus/comanda', 'token=' . $this->session->data['token'], 'SSL');

        // instantiez clasa urgent
        require(DIR_CATALOG.'model/shipping/urgentcargusclass.php');
        $this->model_shipping_urgentcargusclass = new ModelShippingUrgentCargusClass();

        // setez url si key
        $this->model_shipping_urgentcargusclass->SetKeys($this->config->get('urgentcargus_api_url'), $this->config->get('urgentcargus_api_key'));

        // UC login user
        $fields = array(
            'UserName' => $this->config->get('urgentcargus_username'),
            'Password' => $this->config->get('urgentcargus_password')
        );
        $token = $this->model_shipping_urgentcargusclass->CallMethod('LoginUser', $fields, 'POST');

        // selectez awb-ul pentru editare
        $awb = $this->db->query("SELECT * FROM awb_urgent_cargus WHERE id = '".$this->request->get['awb']."' ORDER BY id ASC LIMIT 0, 1");
        $this->data['awb'] = $awb;

        // obtine lista punctelor de ridicare
        $this->data['pickups'] = $this->model_shipping_urgentcargusclass->CallMethod('PickupLocations', array(), 'GET', $token);
        if (is_null($this->data['pickups'])) {
            $this->data['valid'] = false;
            $this->data['error'] = $this->language->get('text_error').'Nu exista niciun punct de ridicare asociat acestui cont!';
        }

        // preiau judetele din opencart
        $this->load->model('localisation/zone');
        $this->data['judete'] = $this->model_localisation_zone->getZonesByCountryId(175);

        // obtin lista de judete din api
        $judete = array();
        $dataJudete = $this->model_shipping_urgentcargusclass->CallMethod('Counties?countryId=1', array(), 'GET', $token);
        foreach ($dataJudete as $val) {
            $judete[strtolower($val['Abbreviation'])] = $val['CountyId'];
        }

        // obtin lista de localitati pe baza abrevierii judetului
        $this->data['localitati'] = $this->model_shipping_urgentcargusclass->CallMethod('Localities?countryId=1&countyId='.$judete[strtolower($this->data['awb']->row['county_name'])], array(), 'GET', $token);

        $this->data['action'] = $this->url->link('urgentcargus/edit/save', 'token=' . $this->session->data['token'].'&awb='.$this->request->get['awb'], 'SSL');

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL')
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_shipping'),
            'href'      => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL')
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_currentorder'),
            'href'      => $this->url->link('urgentcargus/comanda', 'token=' . $this->session->data['token'], 'SSL')
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('urgentcargus/edit', 'token=' . $this->session->data['token'].'&awb='.$this->request->get['awb'], 'SSL')
        );

        $this->template = 'urgentcargus/edit.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
    }

    protected function validate() {
		if (!$this->user->hasPermission('modify', 'urgentcargus/edit')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

    public function save(){
        $this->db->query("UPDATE
                                awb_urgent_cargus
                            SET
                                pickup_id = '".$this->request->post['pickup_id']."',
                                name = '".$this->request->post['name']."',
                                locality_name = '".$this->request->post['city']."',
                                county_name = '".$this->request->post['zone_id']."',
                                address = '".$this->request->post['address']."',
                                contact = '".$this->request->post['contact']."',
                                phone = '".$this->request->post['phone']."',
                                email = '".$this->request->post['email']."',
                                parcels = '".$this->request->post['parcels']."',
                                envelopes = '".$this->request->post['envelopes']."',
                                weight = '".$this->request->post['weight']."',
                                value = '".$this->request->post['value']."',
                                cash_repayment = '".$this->request->post['cash_repayment']."',
                                bank_repayment = '".$this->request->post['bank_repayment']."',
                                other_repayment = '".$this->request->post['other_repayment']."',
                                payer = '".$this->request->post['payer']."',
                                saturday_delivery = '".$this->request->post['saturday_delivery']."',
                                morning_delivery = '".$this->request->post['morning_delivery']."',
                                openpackage = '".$this->request->post['openpackage']."',
                                observations = '".$this->request->post['observations']."',
                                contents = '".$this->request->post['contents']."',
                                shipping_code = '".$this->request->post['shipping_code']."'
                            WHERE
                                id ='".$this->request->get['awb']."'");

        $this->language->load('urgentcargus/edit');
		$this->session->data['success'] = $this->language->get('text_success');
        $this->redirect($this->url->link('urgentcargus/comanda', 'token=' . $this->session->data['token'], 'SSL'));
	}
}
?>