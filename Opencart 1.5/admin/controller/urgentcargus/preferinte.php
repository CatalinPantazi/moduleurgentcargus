<?php
class ControllerUrgentCargusPreferinte extends Controller {
    private $error = array();

    public function index(){

        $this->language->load('urgentcargus/preferinte');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		$this->data['success'] = '';
        $this->data['error'] = '';
        $this->data['error_warning'] = '';
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('urgentcargus_preferinte', $this->request->post);
			$this->data['success'] = $this->language->get('text_success');
		}

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_edit'] = $this->language->get('text_edit');
        $this->data['text_choose_price'] = $this->language->get('text_choose_price');
        $this->data['text_choose_pickup'] = $this->language->get('text_choose_pickup');
        $this->data['text_no'] = $this->language->get('text_no');
        $this->data['text_yes'] = $this->language->get('text_yes');
        $this->data['text_cash'] = $this->language->get('text_cash');
        $this->data['text_bank'] = $this->language->get('text_bank');
        $this->data['text_sender'] = $this->language->get('text_sender');
        $this->data['text_recipient'] = $this->language->get('text_recipient');
        $this->data['text_parcel'] = $this->language->get('text_parcel');
        $this->data['text_envelope'] = $this->language->get('text_envelope');

        // instantiez clasa urgent
        require(DIR_CATALOG.'model/shipping/urgentcargusclass.php');
        $this->model_shipping_urgentcargusclass = new ModelShippingUrgentCargusClass();

        // setez url si key
        $this->model_shipping_urgentcargusclass->SetKeys($this->config->get('urgentcargus_api_url'), $this->config->get('urgentcargus_api_key'));

        // UC login user
        $fields = array(
            'UserName' => $this->config->get('urgentcargus_username'),
            'Password' => $this->config->get('urgentcargus_password')
        );
        $token = $this->model_shipping_urgentcargusclass->CallMethod('LoginUser', $fields, 'POST');

        if (is_array($token)) {
            $this->data['valid'] = false;
            $this->data['error'] = $this->language->get('text_error').$token['data'];
        } else {
            $this->data['valid'] = true;

            // obtine lista planurilor tarifare
            $this->data['prices'] = $this->model_shipping_urgentcargusclass->CallMethod('PriceTables', array(), 'GET', $token);
            if (is_null($this->data['prices'])) {
                $this->data['valid'] = false;
                $this->data['error'] = $this->language->get('text_error').'Nu exista niciun plan tarifar asociat acestui cont!';
            }

            // obtine lista punctelor de ridicare
            $this->data['pickups'] = $this->model_shipping_urgentcargusclass->CallMethod('PickupLocations', array(), 'GET', $token);
            if (is_null($this->data['pickups'])) {
                $this->data['valid'] = false;
                $this->data['error'] = $this->language->get('text_error').'Nu exista niciun punct de ridicare asociat acestui cont!';
            }

            $this->data['entry_price'] = $this->language->get('entry_price');
            $this->data['entry_pickup'] = $this->language->get('entry_pickup');
            $this->data['entry_insurance'] = $this->language->get('entry_insurance');
            $this->data['entry_saturday'] = $this->language->get('entry_saturday');
            $this->data['entry_morning'] = $this->language->get('entry_morning');
            $this->data['entry_openpackage'] = $this->language->get('entry_openpackage');
            $this->data['entry_repayment'] = $this->language->get('entry_repayment');
            $this->data['entry_payer'] = $this->language->get('entry_payer');
            $this->data['entry_type'] = $this->language->get('entry_type');
            $this->data['entry_noextrakm'] = $this->language->get('entry_noextrakm');
            $this->data['entry_noextrakm_details'] = $this->language->get('entry_noextrakm_details');
            $this->data['entry_free'] = $this->language->get('entry_free');
            $this->data['entry_free_details'] = $this->language->get('entry_free_details');
            $this->data['entry_fixed'] = $this->language->get('entry_fixed');
            $this->data['entry_fixed_details'] = $this->language->get('entry_fixed_details');

            $this->data['button_save'] = $this->language->get('button_save');
            $this->data['button_cancel'] = $this->language->get('button_cancel');
            $this->data['cancel'] = $this->url->link('urgentcargus/preferinte', 'token=' . $this->session->data['token'], 'SSL');

            if (isset($this->error['warning'])) {
                $this->data['error_warning'] = $this->error['warning'];
            } else {
                $this->data['error_warning'] = '';
            }

            $this->data['action'] = $this->url->link('urgentcargus/preferinte', 'token=' . $this->session->data['token'], 'SSL');

            if (isset($this->request->post['urgentcargus_preferinte_price'])) {
               $this->data['urgentcargus_preferinte_price'] = $this->request->post['urgentcargus_preferinte_price'];
            } else {
               $this->data['urgentcargus_preferinte_price'] = $this->config->get('urgentcargus_preferinte_price');
            }

            if (isset($this->request->post['urgentcargus_preferinte_pickup'])) {
                $this->data['urgentcargus_preferinte_pickup'] = $this->request->post['urgentcargus_preferinte_pickup'];
            } else {
                $this->data['urgentcargus_preferinte_pickup'] = $this->config->get('urgentcargus_preferinte_pickup');
            }

            if (isset($this->request->post['urgentcargus_preferinte_pickup_locality'])) {
                $this->data['urgentcargus_preferinte_pickup_locality'] = $this->request->post['urgentcargus_preferinte_pickup_locality'];
            } else {
                $this->data['urgentcargus_preferinte_pickup_locality'] = $this->config->get('urgentcargus_preferinte_pickup_locality');
            }

            if (isset($this->request->post['urgentcargus_preferinte_insurance'])) {
                $this->data['urgentcargus_preferinte_insurance'] = $this->request->post['urgentcargus_preferinte_insurance'];
            } else {
                $this->data['urgentcargus_preferinte_insurance'] = $this->config->get('urgentcargus_preferinte_insurance');
            }

            if (isset($this->request->post['urgentcargus_preferinte_saturday'])) {
                $this->data['urgentcargus_preferinte_saturday'] = $this->request->post['urgentcargus_preferinte_saturday'];
            } else {
                $this->data['urgentcargus_preferinte_saturday'] = $this->config->get('urgentcargus_preferinte_saturday');
            }

            if (isset($this->request->post['urgentcargus_preferinte_morning'])) {
                $this->data['urgentcargus_preferinte_morning'] = $this->request->post['urgentcargus_preferinte_morning'];
            } else {
                $this->data['urgentcargus_preferinte_morning'] = $this->config->get('urgentcargus_preferinte_morning');
            }

            if (isset($this->request->post['urgentcargus_preferinte_openpackage'])) {
                $this->data['urgentcargus_preferinte_openpackage'] = $this->request->post['urgentcargus_preferinte_openpackage'];
            } else {
                $this->data['urgentcargus_preferinte_openpackage'] = $this->config->get('urgentcargus_preferinte_openpackage');
            }

            if (isset($this->request->post['urgentcargus_preferinte_repayment'])) {
                $this->data['urgentcargus_preferinte_repayment'] = $this->request->post['urgentcargus_preferinte_repayment'];
            } else {
                $this->data['urgentcargus_preferinte_repayment'] = $this->config->get('urgentcargus_preferinte_repayment');
            }

            if (isset($this->request->post['urgentcargus_preferinte_payer'])) {
                $this->data['urgentcargus_preferinte_payer'] = $this->request->post['urgentcargus_preferinte_payer'];
            } else {
                $this->data['urgentcargus_preferinte_payer'] = $this->config->get('urgentcargus_preferinte_payer');
            }

            if (isset($this->request->post['urgentcargus_preferinte_type'])) {
                $this->data['urgentcargus_preferinte_type'] = $this->request->post['urgentcargus_preferinte_type'];
            } else {
                $this->data['urgentcargus_preferinte_type'] = $this->config->get('urgentcargus_preferinte_type');
            }

            if (isset($this->request->post['urgentcargus_preferinte_free'])) {
                $this->data['urgentcargus_preferinte_free'] = $this->request->post['urgentcargus_preferinte_free'];
            } else {
                $this->data['urgentcargus_preferinte_free'] = $this->config->get('urgentcargus_preferinte_free');
            }

            if (isset($this->request->post['urgentcargus_preferinte_fixed'])) {
                $this->data['urgentcargus_preferinte_fixed'] = $this->request->post['urgentcargus_preferinte_fixed'];
            } else {
                $this->data['urgentcargus_preferinte_fixed'] = $this->config->get('urgentcargus_preferinte_fixed');
            }

            if (isset($this->request->post['urgentcargus_preferinte_noextrakm'])) {
                $this->data['urgentcargus_preferinte_noextrakm'] = $this->request->post['urgentcargus_preferinte_noextrakm'];
            } else {
                $this->data['urgentcargus_preferinte_noextrakm'] = $this->config->get('urgentcargus_preferinte_noextrakm');
            }
        }

        $this->data['breadcrumbs'] = array();
        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL')
        );
        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_shipping'),
            'href'      => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL')
        );
        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('urgentcargus/preferinte', 'token=' . $this->session->data['token'], 'SSL')
        );

        $this->template = 'urgentcargus/preferinte.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
    }

    protected function validate() {
		if (!$this->user->hasPermission('modify', 'urgentcargus/preferinte')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}
?>