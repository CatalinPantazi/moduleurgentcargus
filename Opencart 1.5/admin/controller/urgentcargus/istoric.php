<?php
class ControllerUrgentCargusIstoric extends Controller {
    private $error = array();

    public function index(){

        $this->language->load('urgentcargus/istoric');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->data['success'] = '';
        $this->data['error'] = '';
        $this->data['error_warning'] = '';
        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['text_edit'] = $this->language->get('text_edit');
        $this->data['text_view'] = $this->language->get('text_view');
        $this->data['text_viewawb'] = $this->language->get('text_viewawb');
        $this->data['text_noresults'] = $this->language->get('text_noresults');
        $this->data['text_noresultsawb'] = $this->language->get('text_noresultsawb');
        $this->data['text_nodata'] = $this->language->get('text_nodata');
        $this->data['text_pickup'] = $this->language->get('text_pickup');
        $this->data['text_changepickup'] = $this->language->get('text_changepickup');
        $this->data['text_idcomanda'] = $this->language->get('text_idcomanda');
        $this->data['text_datavalidare'] = $this->language->get('text_datavalidare');
        $this->data['text_intervalridicare'] = $this->language->get('text_intervalridicare');
        $this->data['text_dataprocesare'] = $this->language->get('text_dataprocesare');
        $this->data['text_awburi'] = $this->language->get('text_awburi');
        $this->data['text_plicuri'] = $this->language->get('text_plicuri');
        $this->data['text_colete'] = $this->language->get('text_colete');
        $this->data['text_greutate'] = $this->language->get('text_greutate');
        $this->data['text_status'] = $this->language->get('text_status');
        $this->data['text_serieclient'] = $this->language->get('text_serieclient');
        $this->data['text_numarawb'] = $this->language->get('text_numarawb');
        $this->data['text_numedestinatar'] = $this->language->get('text_numedestinatar');
        $this->data['text_localitatedestinatar'] = $this->language->get('text_localitatedestinatar');
        $this->data['text_cost'] = $this->language->get('text_cost');
        $this->data['text_awb_expeditor'] = $this->language->get('text_awb_expeditor');
        $this->data['text_awb_destinatar'] = $this->language->get('text_awb_destinatar');
        $this->data['text_awb_nume'] = $this->language->get('text_awb_nume');
        $this->data['text_awb_judet'] = $this->language->get('text_awb_judet');
        $this->data['text_awb_localitate'] = $this->language->get('text_awb_localitate');
        $this->data['text_awb_strada'] = $this->language->get('text_awb_strada');
        $this->data['text_awb_numar'] = $this->language->get('text_awb_numar');
        $this->data['text_awb_adresa'] = $this->language->get('text_awb_adresa');
        $this->data['text_awb_perscontact'] = $this->language->get('text_awb_perscontact');
        $this->data['text_awb_telefon'] = $this->language->get('text_awb_telefon');
        $this->data['text_awb_email'] = $this->language->get('text_awb_email');
        $this->data['text_awb_detaliiawb'] = $this->language->get('text_awb_detaliiawb');
        $this->data['text_awb_codbara'] = $this->language->get('text_awb_codbara');
        $this->data['text_awb_valoaredeclarata'] = $this->language->get('text_awb_valoaredeclarata');
        $this->data['text_awb_rambursnumerar'] = $this->language->get('text_awb_rambursnumerar');
        $this->data['text_awb_ramburscont'] = $this->language->get('text_awb_ramburscont');
        $this->data['text_awb_rambursalt'] = $this->language->get('text_awb_rambursalt');
        $this->data['text_awb_deschidere'] = $this->language->get('text_awb_deschidere');
        $this->data['text_awb_livraresambata'] = $this->language->get('text_awb_livraresambata');
        $this->data['text_awb_livraredimineata'] = $this->language->get('text_awb_livraredimineata');
        $this->data['text_awb_plataexpeditie'] = $this->language->get('text_awb_plataexpeditie');
        $this->data['text_awb_observatii'] = $this->language->get('text_awb_observatii');
        $this->data['text_awb_continut'] = $this->language->get('text_awb_continut');
        $this->data['text_awb_serieclient'] = $this->language->get('text_awb_serieclient');
        $this->data['text_da'] = $this->language->get('text_da');
        $this->data['text_nu'] = $this->language->get('text_nu');

        if (isset($_GET['LocationId'])) {
            $pickup = $this->request->get['LocationId'];
        } else {
            $pickup = $this->config->get('urgentcargus_preferinte_pickup');
        }

        $this->data['urgentcargus_preferinte_pickup']  = $pickup;
        $this->data['token'] = $this->session->data['token'];
        $this->data['view_url'] = $this->url->link('urgentcargus/istoric', 'token=' . $this->session->data['token'], 'SSL');

        // instantiez clasa urgent
        require(DIR_CATALOG.'model/shipping/urgentcargusclass.php');
        $this->model_shipping_urgentcargusclass = new ModelShippingUrgentCargusClass();

        // setez url si key
        $this->model_shipping_urgentcargusclass->SetKeys($this->config->get('urgentcargus_api_url'), $this->config->get('urgentcargus_api_key'));

        // UC login user
        $fields = array(
            'UserName' => $this->config->get('urgentcargus_username'),
            'Password' => $this->config->get('urgentcargus_password')
        );
        $token = $this->model_shipping_urgentcargusclass->CallMethod('LoginUser', $fields, 'POST');

        if (is_array($token)) {
            $this->data['valid'] = false;
            $this->data['error'] = $this->language->get('text_error').$token['data'];
        } else {
            $this->data['valid'] = true;

            if (isset($_GET['BarCode'])) {
                $this->data['zone'] = 'awb_details';

                // UC get detalii awb
                $this->data['detaliuAwb'] = $this->model_shipping_urgentcargusclass->CallMethod('Awbs?&barCode=' . $this->request->get['BarCode'], array(), 'GET', $token);
                if (is_array($this->data['detaliuAwb']) && count($this->data['detaliuAwb']) == 1) {
                    $this->data['detaliuAwb'] = $this->data['detaliuAwb'][0];
                }
            } elseif (isset($_GET['OrderId'])) {
                $this->data['zone'] = 'awbs';
                $this->data['OrderId'] = $this->request->get['OrderId'];

                // UC get istoric awb-uri comanda
                $this->data['awbs'] = $this->model_shipping_urgentcargusclass->CallMethod('Awbs?&orderId=' . $this->request->get['OrderId'], array(), 'GET', $token);
            } else {
                $this->data['zone'] = 'orders';

                // obtine lista punctelor de ridicare
                $this->data['pickups'] = $this->model_shipping_urgentcargusclass->CallMethod('PickupLocations', array(), 'GET', $token);
                if (is_null($this->data['pickups'])) {
                    $this->data['valid'] = false;
                    $this->data['error'] = $this->language->get('text_error').'Nu exista niciun punct de ridicare asociat acestui cont!';
                }

                // UC get istoric comenzi
                $this->data['orders'] = $this->model_shipping_urgentcargusclass->CallMethod('Orders?locationId='.$pickup.'&status=1&pageNumber=1&itemsPerPage=100', array(), 'GET', $token);
            }
        }

        $this->data['breadcrumbs'] = array();
        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL')
        );
        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_shipping'),
            'href'      => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL')
        );
        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('urgentcargus/istoric', 'token=' . $this->session->data['token'] . '&LocationId=' .$pickup, 'SSL')
        );

        if (isset($_GET['OrderId'])) {
            $this->data['breadcrumbs'][] = array(
                'text'      => $this->language->get('text_orderdetails'),
                'href'      => $this->url->link('urgentcargus/istoric', 'token=' . $this->session->data['token'] . '&LocationId=' .$pickup . '&OrderId=' . $this->request->get['OrderId'], 'SSL')
            );
        }

        if (isset($_GET['BarCode'])) {
            $this->data['breadcrumbs'][] = array(
                'text'      => $this->language->get('text_awbdetails'),
                'href'      => $this->url->link('urgentcargus/istoric', 'token=' . $this->session->data['token'] . '&LocationId=' .$pickup . '&OrderId=' . $this->request->get['OrderId'] . '&BarCode=' . $this->request->get['BarCode'], 'SSL')
            );
        }

        $this->template = 'urgentcargus/istoric.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
    }
}
?>