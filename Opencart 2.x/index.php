<?php
function getFiles($dir) {
    $files = array();
    $root = array_diff(scandir($dir), array(
        '..',
        '.'
    ));
    foreach ($root as $r) {
        $files[] = $dir.'\\'.$r;
        if (is_dir($dir.'/'.$r)) {
            $files = array_merge($files, getFiles($dir.'\\'.$r));
        }
    }
    return $files;
}

$files = array();
$root = array_diff(scandir(getcwd()), array(
    //'install.xml', // to be deleted
    '..',
    '.',
    'OpenCart 2.x.phpproj',
    'OpenCart 2.x.phpproj.user',
    'OpenCart 2.x.phpproj.vspscc',
    'UrgentCargus.ocmod.zip',
    'Properties',
    'index.php'
));
foreach ($root as $r) {
    $files[] = $r;
    if (is_dir($r)) {
        $files = array_merge($files, getFiles($r));
    }
}

$zip = new ZipArchive;
if($zip->open('UrgentCargus.ocmod.zip', ZIPARCHIVE::OVERWRITE) !== true) {
    echo 'failed';
} else {
    foreach ($files as $f) {
        if (is_dir($f)) {
            $zip->addEmptyDir(str_replace('\\', '/', $f));
        } else {
            $zip->addFile(str_replace('\\', '/', $f));
        }
    }
    $zip->close();
    echo 'ok';
}
?>