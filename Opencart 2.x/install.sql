CREATE TABLE IF NOT EXISTS `awb_urgent_cargus` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `order_id` int(11) NOT NULL,
    `pickup_id` int(11) NOT NULL,
    `name` varchar(64) NOT NULL,
    `locality_name` varchar(128) NOT NULL,
    `county_name` varchar(128) NOT NULL,
    `street_name` varchar(128) NOT NULL,
    `number` int(11) NOT NULL,
    `address` varchar(256) NOT NULL,
    `postcode` varchar(64) NOT NULL,
    `contact` varchar(64) NOT NULL,
    `phone` varchar(32) NOT NULL,
    `email` varchar(96) NOT NULL,
    `parcels` int(11) NOT NULL,
    `envelopes` int(11) NOT NULL,
    `weight` int(11) NOT NULL,
    `value` double NOT NULL,
    `cash_repayment` double NOT NULL,
    `bank_repayment` double NOT NULL,
    `other_repayment` varchar(256) NOT NULL,
    `payer` int(11) NOT NULL,
    `saturday_delivery` tinyint(1) NOT NULL,
	`morning_delivery` tinyint(1) NOT NULL,
	`openpackage` tinyint(1) NOT NULL,
    `observations` varchar(256) NOT NULL,
    `contents` varchar(256) NOT NULL,
    `barcode` varchar(50) NOT NULL,
    `shipping_code` varchar(256) NOT NULL,
    `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;