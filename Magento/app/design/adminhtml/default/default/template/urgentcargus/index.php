<div class="content-header">
    <h3>Urgent Cargus - AWB-uri in asteptare</h3>
</div>
<?php if (!Urgent_Cargus_Adminhtml_IndexController::checkCredentials()) { echo 'Userul sau parola contului de utilizator folosit nu sunt corecte!'; } else { ?>
<?php
          $listaAsteptare = Urgent_Cargus_Adminhtml_IndexController::getAwbsAsteptare();
          $puncteRidicare = Urgent_Cargus_Adminhtml_IndexController::getPickupPoints();
          $puncteRidicareFormated = array();
          foreach ($puncteRidicare as $pr) {
              $puncteRidicareFormated[$pr['LocationId']] = $pr['Name'];
          }
?>
<div class="entry-edit">
    <?php if (count($listaAsteptare) == 0) { echo 'Nu exista niciun AWB in asteptare!'; } else { ?>
    <form id="indexAsteptareForm" name="indexAsteptareForm" method="post" action="<?php echo $this->getUrl('*/*/postIndexAsteptare'); ?>">
        <input name="form_key" type="hidden" value="<?php echo Mage::getSingleton('core/session')->getFormKey() ?>" />
        <div class="grid">
            <table cellspacing="0" class="data">
                <thead>
                    <tr class="headings">
                        <th class="no-link" style="width:13px"><input type="checkbox" name="null" class="checkbox_master" rel="grup1" /></th>
                        <th class="no-link">ID Comanda</th>
                        <th class="no-link">Punct de ridicare</th>
                        <th class="no-link">Nume destinatar</th>
                        <th class="no-link">Localitate destinatar</th>
                        <th class="no-link">Plicuri</th>
                        <th class="no-link">Colete</th>
                        <th class="no-link">Greutare</th>
                        <th class="no-link">Ramburs numerar</th>
                        <th class="no-link">Ramburs cont</th>
                        <th class="no-link">Platitor expeditie</th>
                        <th class="no-link last" style="width:38px"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($listaAsteptare as $item) { ?>
                    <tr>
                        <td><input class="grup1" type="checkbox" name="items[]" value="<?php echo $item['orderId']; ?>"></td>
                        <td><?php echo $item['orderId']; ?></td>
                        <td><?php echo $puncteRidicareFormated[$item['pickupLocationId']]; ?></td>
                        <td><?php echo $item['numeDest']; ?></td>
                        <td><?php echo $item['localitateDest'].($item['judetDest'] ? ', ' : '').$item['judetDest']; ?></td>
                        <td><?php echo $item['plicuri']; ?></td>
                        <td><?php echo $item['colete']; ?></td>
                        <td><?php echo $item['kilograme']; ?> kg</td>
                        <td><?php echo $item['rambursNumerar']; ?> lei</td>
                        <td><?php echo $item['rambursCont']; ?> lei</td>
                        <td><?php echo ($item['platitorExpeditie'] == 2 ? 'Destinatar' : 'Expeditor'); ?></td>
                        <td class="last"><a href="<?php echo $this->getUrl('*/*/editawb'); ?>?OrderId=<?php echo $item['orderId']; ?>">Editare</a></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <button onclick="indexAsteptareForm.submit()" class="scalable save" type="button"><span>Valideaza AWB-urile bifate</span></button>
        <button onclick="postDeleteAsteptare()" class="scalable back" type="button"><span>Sterge AWB-urile bifate</span></button>
    </form>
    <script type="text/javascript">
        var indexAsteptareForm = new varienForm('indexAsteptareForm');

        function postDeleteAsteptare() {
            jQuery('#indexAsteptareForm').attr('action', '<?php echo $this->getUrl('*/*/postIndexAsteptareDelete'); ?>').submit();
        }
    </script>
    <?php } ?>
</div><br/><br/>

<div class="content-header">
    <h3>Urgent Cargus - AWB-uri validate pentru punctul de lucru</h3>
</div>
<?php
          $pickupPoints = Urgent_Cargus_Adminhtml_IndexController::getPickupPoints();
          $LocationId = Mage::getStoreConfig('urgentcargus/pickup_id');
          if (isset($_GET['LocationId'])) {
              $LocationId = addslashes($_GET['LocationId']);
          }
          $listaValidate = Urgent_Cargus_Adminhtml_IndexController::getCurrentOrder($LocationId);
?>
<form id="pickup_form" method="get">
    <select name="LocationId" style="padding: 3px;">
        <?php
          foreach ($pickupPoints as $pick) {
              echo '<option value="'.$pick['LocationId'].'" '.($LocationId == $pick['LocationId'] ? 'selected="selected"' : '').'>'.$pick['Name'].'</option>';
          }
        ?>
    </select>
    <script>
        jQuery(function () {
            jQuery('select[name="LocationId"]').change(function () {
                jQuery('#pickup_form').submit();
            });
        });
    </script>
    <style>
        #pickup_form {
            position: absolute;
            margin: -46px 0 0 410px;
        }
    </style>
</form>
<div class="entry-edit">
    <?php if (count($listaValidate) == 0) { echo 'Nu exista niciun AWB validat pentru acest punct de lucru!'; } else { ?>
    <form id="indexValidateForm" name="indexValidateForm" method="post" action="<?php echo $this->getUrl('*/*/postIndexValidateDelete'); ?>">
        <input name="form_key" type="hidden" value="<?php echo Mage::getSingleton('core/session')->getFormKey() ?>" />
        <div class="grid">
            <table cellspacing="0" class="data">
                <thead>
                    <tr class="headings">
                        <th class="no-link" style="width:13px"><input type="checkbox" name="null" class="checkbox_master" rel="grup2" /></th>
                        <th class="no-link">ID Comanda</th>
                        <th class="no-link">Cod bara AWB</th>
                        <th class="no-link">Cost expeditie</th>
                        <th class="no-link">Nume destinatar</th>
                        <th class="no-link">Localitate destinatar</th>
                        <th class="no-link">Plicuri</th>
                        <th class="no-link">Colete</th>
                        <th class="no-link">Greutare</th>
                        <th class="no-link">Ramburs numerar</th>
                        <th class="no-link">Ramburs cont</th>
                        <th class="no-link">Platitor expeditie</th>
                        <th class="no-link last">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($listaValidate as $item) { ?>
                    <tr>
                        <td><input class="grup2" type="checkbox" name="items[]" value="<?php echo $item['BarCode']; ?>"></td>
                        <td><?php echo $item['CustomString']; ?></td>
                        <td><?php echo $item['BarCode']; ?></td>
                        <td><?php echo $item['ShippingCost']['GrandTotal']; ?> lei</td>
                        <td><?php echo $item['Recipient']['Name']; ?></td>
                        <td><?php echo $item['Recipient']['LocalityName'].($item['Recipient']['CountyName'] ? ', ' : '').$item['Recipient']['CountyName']; ?></td>
                        <td><?php echo $item['Envelopes']; ?></td>
                        <td><?php echo $item['Parcels']; ?></td>
                        <td><?php echo $item['TotalWeight']; ?> kg</td>
                        <td><?php echo $item['CashRepayment']; ?> lei</td>
                        <td><?php echo $item['BankRepayment']; ?> lei</td>
                        <td><?php echo ($item['ShipmentPayer'] == 2 ? 'Destinatar' : 'Expeditor'); ?></td>
                        <td class="last"><?php echo $item['Status']; ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <button onclick="printValidate()" class="scalable save" type="button"><span>Printeaza AWB-urile bifate</span></button>
        <button onclick="finalizareComanda()" class="scalable save" type="button"><span>Finalizeaza comanda</span></button>
        <button onclick="indexValidateForm.submit()" class="scalable back" type="button"><span>Anuleaza AWB-urile bifate</span></button>
    </form>
    <script type="text/javascript">
        var indexValidateForm = new varienForm('indexValidateForm');

        function printValidate() {
            var coduri = new Array();
            jQuery('input.grup2[name*=\'items\']:checked').each(function () {
                coduri.push(jQuery(this).val());
            });
            if (coduri.length > 0) {
                var url = "<?php echo $this->getUrl('*/*/postIndexAwbprint'); ?>";
                var coduri_string = "[" + coduri.join(",") + "]";
                window.open(url + "?bar_codes=" + coduri_string, "", "width=900, height=600, left=50, top=50");
                location.reload();
            }
            return false;
        }

        function finalizareComanda() {
            var url = "<?php echo $this->getUrl('*/*/sendorder'); ?>";
            window.open(url + "?LocationId=<?php echo $LocationId; ?>", "", "width=900, height=380, left=50, top=50");
            return false;
        }
    </script>
    <?php } ?>
</div>

<style>
    input.grup1, input.grup2 {
        margin-left: 1px;
    }
</style>
<script>
    jQuery(function () {
        jQuery(".checkbox_master").change(function () {
            var rel = jQuery(this).attr('rel');
            jQuery('.' + rel).attr("checked", this.checked);
        });
        jQuery(".grup1").change(function () {
            jQuery('.checkbox_master[rel="grup1"]').attr("checked", jQuery(".grup1:checked").length == jQuery(".grup1").length);
        });
        jQuery(".grup2").change(function () {
            jQuery('.checkbox_master[rel="grup2"]').attr("checked", jQuery(".grup2:checked").length == jQuery(".grup2").length);
        });
    });
</script>
<?php } ?>