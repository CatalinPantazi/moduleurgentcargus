<div class="content-header">
    <?php
    if (isset($_GET['BarCode'])) {
        echo '<h3>Urgent Cargus - Detaliu AWB '.addslashes($_GET['BarCode']).'</h3>';
    } elseif (isset($_GET['OrderId'])) {
        echo '<h3>Urgent Cargus - ISTORIC AWB-uri pentru comanda '.addslashes($_GET['OrderId']).'</h3>';
    } else {
        echo '<h3>Urgent Cargus - ISTORIC expeditii pentru punctul de lucru</h3>';
    }
    ?>
</div>
<?php
$listaComenzi = null;
$listaAwburi = null;
$detaliuAwb = null;

if (isset($_GET['BarCode'])) {
    $BarCode = addslashes($_GET['BarCode']);
    $detaliuAwb = Urgent_Cargus_Adminhtml_IndexController::getAwbByBarcode($BarCode);
    if (is_array($detaliuAwb) && count($detaliuAwb) == 1) {
        $detaliuAwb = $detaliuAwb[0];
    }
} elseif (isset($_GET['OrderId'])) {
    $OrderId = addslashes($_GET['OrderId']);
    $listaAwburi = Urgent_Cargus_Adminhtml_IndexController::getAwbsByOrderId($OrderId);
} else {
    $pickupPoints = Urgent_Cargus_Adminhtml_IndexController::getPickupPoints();
    $go = false;
    if (is_array($pickupPoints)) {
        if (count($pickupPoints) > 0 && !isset($pickupPoints['error'])) {
            $go = true;
        }
    }
    $LocationId = Mage::getStoreConfig('urgentcargus/pickup_id');
    if (isset($_GET['LocationId'])) {
        $LocationId = addslashes($_GET['LocationId']);
    }
    $listaComenzi = Urgent_Cargus_Adminhtml_IndexController::getOrderHistory($LocationId);
}
?>
<div class="entry-edit">
    <?php
    if (!Urgent_Cargus_Adminhtml_IndexController::checkCredentials()) { echo 'Userul sau parola contului de utilizator folosit nu sunt corecte!'; } else {
    ?>
    
    <?php if (!isset($_GET['OrderId']) && !isset($_GET['BarCode'])) { ?>
    <form id="pickup_form" method="get">
        <select name="LocationId">
            <?php
            foreach ($pickupPoints as $pick) {
                echo '<option value="'.$pick['LocationId'].'" '.($LocationId == $pick['LocationId'] ? 'selected="selected"' : '').'>'.$pick['Name'].' | '.$pick['LocalityName'].'</option>';
            }
            ?>
        </select>
        <script>
            jQuery(function () {
                jQuery('select[name="LocationId"]').change(function () {
                    jQuery('#pickup_form').submit();
                });
            });
        </script>
        <style>
            #pickup_form {
                position: absolute;
                margin: -46px 0 0 415px;
            }
            #pickup_form select {
                padding: 3px;
            }
        </style>
    </form>
    <?php } ?>

    <div class="grid">
        <!-- LISTA ISTORIC COMENZI -->
        <?php if ($listaComenzi != null) { ?>
        <?php if (!$go) { echo 'Pe acest cont de utilizator nu este definit niciun punct de ridicare!<br>Logati-va pe site-ul Urgent Cargus si asignati un punct de ridicare acestui cont.'; } ?>
        <?php if (isset($listaComenzi['error'])) { echo $listaComenzi['error']; } else { if (count($listaComenzi) == 0) { echo 'Nu exista nicio comanda expediata din acest punct de lucru!'; } else { ?>
        <table cellspacing="0" class="data">
            <thead>
                <tr class="headings">
                    <th class="no-link">ID Comanda</th>
                    <th class="no-link">Data validare</th>
                    <th class="no-link">Interval ridicare</th>
                    <th class="no-link">Data procesare</th>
                    <th class="no-link">Awb-uri</th>
                    <th class="no-link">Plicuri</th>
                    <th class="no-link">Colete</th>
                    <th class="no-link">Greutare</th>
                    <th class="no-link">Status</th>
                    <th class="no-link last"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($listaComenzi as $item) { ?>
                <tr>
                    <td><?php echo $item['OrderId']; ?></td>
                    <td><?php echo $item['ValidationDate'] ? date('d.m.Y', strtotime($item['ValidationDate'])) : '-'; ?></td>
                    <td><?php echo $item['PickupStartDate'] ? date('d.m.Y H:i', strtotime($item['PickupStartDate']))  . ' - ' . date('H:i', strtotime($item['PickupEndDate'])) : '-'; ?></td>
                    <td><?php echo $item['ProcessedDate'] ? date('d.m.Y', strtotime($item['ProcessedDate'])) : '-'; ?></td>
                    <td><?php echo $item['NoAwb']; ?></td>
                    <td><?php echo $item['NoEnvelop']; ?></td>
                    <td><?php echo $item['NoParcel']; ?></td>
                    <td><?php echo $item['TotalWeight']; ?></td>
                    <td><?php echo $item['OrdStatus']; ?></td>
                    <td class="last"><?php echo $item['NoAwb'] > 0 ? '<a style="text-decoration:none" href="?OrderId='.$item['OrderId'].'">Afiseaza</a>' : ''; ?></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php echo '<span style="color:#999;font-size:11px;">Sunt afisate ultimele '.(count($listaComenzi)).' comenzi expediate din punctul curent de ridicare!</span>'; ?>
        <?php } } ?>
        <?php } ?>

        <!-- LISTA ISTORIC AWB-URI -->
        <?php if ($listaAwburi != null) { ?>
        <?php if (isset($listaAwburi['error'])) { echo $listaAwburi['error']; } else { if (count($listaAwburi) == 0) { echo 'Nu exista nicio comanda expediata din acest punct de lucru!'; } else { ?>
        <table cellspacing="0" class="data">
            <thead>
                <tr class="headings">
                    <th class="no-link">ID comanda</th>
                    <th class="no-link">Serie AWB</th>
                    <th class="no-link">Cost livrare</th>
                    <th class="no-link">Nume destinatar</th>
                    <th class="no-link">Localitate destinatar</th>
                    <th class="no-link">Plicuri</th>
                    <th class="no-link">Colete</th>
                    <th class="no-link">Greutare</th>
                    <th class="no-link">Ramburs numerar</th>
                    <th class="no-link">Ramburs cont colector</th>
                    <th class="no-link">Platitor expeditie</th>
                    <th class="no-link">Status</th>
                    <th class="no-link last"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($listaAwburi as $item) { ?>
                <tr>
                    <td><?php echo $item['CustomString']; ?></td>
                    <td><?php echo '<a target="_blank" style="text-decoration:none; color:#000;" href="https://www.urgentcargus.ro/Private/Tracking.aspx?CodBara='.$item['BarCode'].'">'.$item['BarCode'].'</a>'; ?></td>
                    <td><?php echo $item['ShippingCost']['GrandTotal']; ?> lei</td>
                    <td><?php echo $item['Recipient']['Name']; ?></td>
                    <td><?php echo $item['Recipient']['LocalityName']; ?></td>
                    <td><?php echo $item['Envelopes']; ?></td>
                    <td><?php echo $item['Parcels']; ?></td>
                    <td><?php echo $item['TotalWeight']; ?></td>
                    <td><?php echo $item['CashRepayment']; ?> lei</td>
                    <td><?php echo $item['BankRepayment']; ?> lei</td>
                    <td><?php echo $item['ShipmentPayer'] == 2 ? 'Destinatar' : 'Expeditor';; ?></td>
                    <td><?php echo $item['Status']; ?></td>
                    <td class="last"><?php echo '<a style="text-decoration:none" href="?BarCode='.$item['BarCode'].'">Afiseaza</a>'; ?></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php } } ?>
        <?php } ?>

        <!-- LISTA DETALII AWB -->
        <?php if ($detaliuAwb != null) { ?>
        <?php if (is_array($detaliuAwb) && isset($detaliuAwb['error'])) { echo $detaliuAwb['error']; } else { ?>
        <table cellspacing="0" class="data">
            <tbody>
                <tr class="headings"><th colspan="2" class="no-link last">Expeditor</th></tr>
                <tr><td style="width:150px">Nume</td><td class="last"><?php echo $detaliuAwb['Sender']['Name'] ? $detaliuAwb['Sender']['Name'] : '-'; ?></td></tr>
                <tr><td>Judet</td><td class="last"><?php echo $detaliuAwb['Sender']['CountyName'] ? $detaliuAwb['Sender']['CountyName'] : '-'; ?></td></tr>
                <tr><td>Localitate</td><td class="last"><?php echo $detaliuAwb['Sender']['LocalityName'] ? $detaliuAwb['Sender']['LocalityName'] : '-'; ?></td></tr>
                <tr><td>Strada</td><td class="last"><?php echo $detaliuAwb['Sender']['StreetName'] ? $detaliuAwb['Sender']['StreetName'] : '-'; ?></td></tr>
                <tr><td>Numar</td><td class="last"><?php echo $detaliuAwb['Sender']['BuildingNumber'] ? $detaliuAwb['Sender']['BuildingNumber'] : '-'; ?></td></tr>
                <tr><td>Adresa</td><td class="last"><?php echo $detaliuAwb['Sender']['AddressText'] ? $detaliuAwb['Sender']['AddressText'] : '-'; ?></td></tr>
                <tr><td>Persoana contact</td><td class="last"><?php echo $detaliuAwb['Sender']['ContactPerson'] ? $detaliuAwb['Sender']['ContactPerson'] : '-'; ?></td></tr>
                <tr><td>Telefon</td><td class="last"><?php echo $detaliuAwb['Sender']['PhoneNumber'] ? $detaliuAwb['Sender']['PhoneNumber'] : '-'; ?></td></tr>
                <tr><td>Email</td><td class="last"><?php echo $detaliuAwb['Sender']['Email'] ? $detaliuAwb['Sender']['Email'] : '-'; ?></td></tr>

                <tr class="headings"><th colspan="2" class="no-link last">Destinatar</th></tr>
                <tr><td>Nume</td><td class="last"><?php echo $detaliuAwb['Recipient']['Name'] ? $detaliuAwb['Recipient']['Name'] : '-'; ?></td></tr>
                <tr><td>Judet</td><td class="last"><?php echo $detaliuAwb['Recipient']['CountyName'] ? $detaliuAwb['Recipient']['CountyName'] : '-'; ?></td></tr>
                <tr><td>Localitate</td><td class="last"><?php echo $detaliuAwb['Recipient']['LocalityName'] ? $detaliuAwb['Recipient']['LocalityName'] : '-'; ?></td></tr>
                <tr><td>Strada</td><td class="last"><?php echo $detaliuAwb['Recipient']['StreetName'] ? $detaliuAwb['Recipient']['StreetName'] : '-'; ?></td></tr>
                <tr><td>Numar</td><td class="last"><?php echo $detaliuAwb['Recipient']['BuildingNumber'] ? $detaliuAwb['Recipient']['BuildingNumber'] : '-'; ?></td></tr>
                <tr><td>Adresa</td><td class="last"><?php echo $detaliuAwb['Recipient']['AddressText'] ? $detaliuAwb['Recipient']['AddressText'] : '-'; ?></td></tr>
                <tr><td>Persoana contact</td><td class="last"><?php echo $detaliuAwb['Recipient']['ContactPerson'] ? $detaliuAwb['Recipient']['ContactPerson'] : '-'; ?></td></tr>
                <tr><td>Telefon</td><td class="last"><?php echo $detaliuAwb['Recipient']['PhoneNumber'] ? $detaliuAwb['Recipient']['PhoneNumber'] : '-'; ?></td></tr>
                <tr><td>Email</td><td class="last"><?php echo $detaliuAwb['Recipient']['Email'] ? $detaliuAwb['Recipient']['Email'] : '-'; ?></td></tr>

                <tr class="headings"><th colspan="2" class="no-link last">Detalii AWB</th></tr>
                <tr><td>Cod bara</td><td class="last"><?php echo $detaliuAwb['BarCode'] ? $detaliuAwb['BarCode'] : '-'; ?></td></tr>
                <tr><td>Plicuri</td><td class="last"><?php echo $detaliuAwb['Envelopes'] ? $detaliuAwb['Envelopes'] : '-'; ?></td></tr>
                <tr><td>Colete</td><td class="last"><?php echo $detaliuAwb['Parcels'] ? $detaliuAwb['Parcels'] : '-'; ?></td></tr>
                <tr><td>Greutate</td><td class="last"><?php echo $detaliuAwb['TotalWeight'] ? $detaliuAwb['TotalWeight'] : '-'; ?> kg</td></tr>
                <tr><td>Valoare declarata</td><td class="last"><?php echo $detaliuAwb['DeclaredValue'] ? $detaliuAwb['DeclaredValue'] : '0'; ?> lei</td></tr>
                <tr><td>Ramburs numerar</td><td class="last"><?php echo $detaliuAwb['CashRepayment'] ? $detaliuAwb['CashRepayment'] : '0'; ?> lei</td></tr>
                <tr><td>Ramburs cont colector</td><td class="last"><?php echo $detaliuAwb['BankRepayment'] ? $detaliuAwb['BankRepayment'] : '0'; ?> lei</td></tr>
                <tr><td>Ramburs alt tip</td><td class="last"><?php echo $detaliuAwb['OtherRepayment'] ? $detaliuAwb['OtherRepayment'] : '-'; ?></td></tr>
                <tr><td>Deschidere colet</td><td class="last"><?php echo $detaliuAwb['OpenPackage'] ? 'Da' : 'Nu'; ?></td></tr>
                <tr><td>Livrare sambata</td><td class="last"><?php echo $detaliuAwb['SaturdayDelivery'] ? 'Da' : 'Nu'; ?></td></tr>
                <tr><td>Livrare dimineata</td><td class="last"><?php echo $detaliuAwb['MorningDelivery'] ? 'Da' : 'Nu'; ?></td></tr>
                <tr><td>Plata expeditie</td><td class="last"><?php echo $detaliuAwb['ShipmentPayer'] == 1 ? 'Expeditor' : ($detaliuAwb['ShipmentPayer'] == 2 ? 'Destinatar' : 'Tert'); ?></td></tr>
                <tr><td>Observatii</td><td class="last"><?php echo $detaliuAwb['Observations'] ? $detaliuAwb['Observations'] : '-'; ?></td></tr>
                <tr><td>Continut</td><td class="last"><?php echo $detaliuAwb['PackageContent'] ? $detaliuAwb['PackageContent'] : '-'; ?></td></tr>
                <tr><td>Serie client</td><td class="last"><?php echo $detaliuAwb['CustomString'] ? $detaliuAwb['CustomString'] : '-'; ?></td></tr>
                <tr><td>Status</td><td class="last"><?php echo $detaliuAwb['Status'] ? $detaliuAwb['Status'] : '-'; ?></td></tr>
            </tbody>
        </table>
        <?php } ?>
        <?php } ?>
    </div>
    <?php } ?>
</div>