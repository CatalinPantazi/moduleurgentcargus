<div class="content-header">
    <h3>Urgent Cargus - PREFERINTE</h3>
</div>
<?php
$pickupPoints = Urgent_Cargus_Adminhtml_IndexController::getPickupPoints();
$go = false;
if (is_array($pickupPoints)) {
    if (count($pickupPoints) > 0 && !isset($pickupPoints['error'])) {
        $go = true;
    }
}

$pricePlans = Urgent_Cargus_Adminhtml_IndexController::getPricePlans();
?>
<div class="entry-edit">
    <?php
    if (!Urgent_Cargus_Adminhtml_IndexController::checkCredentials()) { echo 'Userul sau parola contului de utilizator folosit nu sunt corecte!'; } else {
        if (!$go) { echo 'Pe acest cont de utilizator nu este definit niciun punct de ridicare!<br>Logati-va pe site-ul Urgent Cargus si asignati un punct de ridicare acestui cont.'; } else {
    ?>
    <form id="preferinteForm" name="preferinteForm" method="post" action="<?php echo $this->getUrl('*/*/postPreferinte'); ?>">
        <input name="form_key" type="hidden" value="<?php echo Mage::getSingleton('core/session')->getFormKey() ?>" />
        <fieldset>
            <table cellspacing="0" class="form-list" width="100%">
                <tr>
                    <td class="label">Planul tarifar</td>
                    <td class="value">
                        <select name="price_id">
                            <option value="0"></option>
                            <?php
                            foreach ($pricePlans as $price) {
                                echo '<option value="'.$price['PriceTableId'].'" '.(Mage::getStoreConfig('urgentcargus/price_id') == $price['PriceTableId'] ? 'selected="selected"' : '').'>'.$price['Name'].'</option>';
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="label">Punctul de ridicare <span class="required">*</span></td>
                    <td class="value">
                        <select name="pickup_id">
                            <option localityId="" value=""></option>
                            <?php
                            foreach ($pickupPoints as $pick) {
                                echo '<option value="'.$pick['LocationId'].'" '.(Mage::getStoreConfig('urgentcargus/pickup_id') == $pick['LocationId'] ? 'selected="selected"' : '').'>'.$pick['Name'].'</option>';
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="label">Deschidere colet</td>
                    <td class="value">
                        <select name="openpackage">
                            <option value="0" <?php if (Mage::getStoreConfig('urgentcargus/openpackage') != 1) echo 'selected="selected"'; ?>>Nu</option>
                            <option value="1" <?php if (Mage::getStoreConfig('urgentcargus/openpackage') == 1) echo 'selected="selected"'; ?>>Da</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="label">Asigurare</td>
                    <td class="value">
                        <select name="insurance">
                            <option value="0" <?php if (Mage::getStoreConfig('urgentcargus/insurance') != 1) echo 'selected="selected"'; ?>>Nu</option>
                            <option value="1" <?php if (Mage::getStoreConfig('urgentcargus/insurance') == 1) echo 'selected="selected"'; ?>>Da</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="label">Livrare sambata</td>
                    <td class="value">
                        <select name="saturday">
                            <option value="0" <?php if (Mage::getStoreConfig('urgentcargus/saturday') != 1) echo 'selected="selected"'; ?>>Nu</option>
                            <option value="1" <?php if (Mage::getStoreConfig('urgentcargus/saturday') == 1) echo 'selected="selected"'; ?>>Da</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="label">Livrare dimineata</td>
                    <td class="value">
                        <select name="morning">
                            <option value="0" <?php if (Mage::getStoreConfig('urgentcargus/morning') != 1) echo 'selected="selected"'; ?>>Nu</option>
                            <option value="1" <?php if (Mage::getStoreConfig('urgentcargus/morning') == 1) echo 'selected="selected"'; ?>>Da</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="label">Ramburs</td>
                    <td class="value">
                        <select name="rapayment">
                            <option value="cash" <?php if (Mage::getStoreConfig('urgentcargus/rapayment') != 'bank') echo 'selected="selected"'; ?>>Numerar</option>
                            <option value="bank" <?php if (Mage::getStoreConfig('urgentcargus/rapayment') == 'bank') echo 'selected="selected"'; ?>>Cont colector</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="label">Platitor</td>
                    <td class="value">
                        <select name="payer">
                            <option value="sender" <?php if (Mage::getStoreConfig('urgentcargus/payer') != 'recipient') echo 'selected="selected"'; ?>>Expeditor</option>
                            <option value="recipient" <?php if (Mage::getStoreConfig('urgentcargus/payer') == 'recipient') echo 'selected="selected"'; ?>>Destinatar</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="label">Localitati</td>
                    <td class="value">
                        <select name="nomenclator">
                            <option value="server" <?php if (Mage::getStoreConfig('urgentcargus/nomenclator') != 'local') echo 'selected="selected"'; ?>>Apeleaza serverul pentru obtinerea localitatilor</option>
                            <option value="local" <?php if (Mage::getStoreConfig('urgentcargus/nomenclator') == 'local') echo 'selected="selected"'; ?>>Utilizeaza lista de localitati salvata local</option>
                        </select>
                        <?php if (Mage::getStoreConfig('urgentcargus/nomenclator') == 'local') { ?>
                            <button onclick="do_preferinte('cu')" class="scalable save" type="button"><span>Actualizeaza</span></button>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td class="label">Tip expeditie implicita</td>
                    <td class="value">
                        <select name="type">
                            <option value="parcel" <?php if (Mage::getStoreConfig('urgentcargus/type') != 'envelope') echo 'selected="selected"'; ?>>Colet</option>
                            <option value="envelope" <?php if (Mage::getStoreConfig('urgentcargus/type') == 'envelope') echo 'selected="selected"'; ?>>Plic</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="label">Plafon transport gratuit</td>
                    <td class="value">
                        <input type="text" name="plafon" value="<?php echo Mage::getStoreConfig('urgentcargus/plafon'); ?>" style="width:60px;" />
                        <span style="color:#666; font-size:11px;">(daca valoarea comenzii depaseste suma in lei introdusa, transportul va fi gratuit pentru destinatar)</span>
                    </td>
                </tr>
                <tr>
                    <td class="label">Cost fix transport</td>
                    <td class="value">
                        <input type="text" name="costfix" value="<?php echo Mage::getStoreConfig('urgentcargus/costfix'); ?>" style="width:60px;" />
                        <span style="color:#666; font-size:11px;">(costul fix de livrare se va aplica indiferent de numarul de kilometri suplimentari si de greutatea totala)</span>
                    </td>
                </tr>
            </table>
        </fieldset>
        <button onclick="do_preferinte('fara')" class="scalable save" type="button"><span>Salveaza preferintele</span></button>
        <input id="update_nomenclator" type="hidden" name="update_nomenclator" value="0" />
    </form>
    <script>
        function do_preferinte(type) {
            if (type == 'cu') {
                document.getElementById('update_nomenclator').value = '1';
            } else {
                document.getElementById('update_nomenclator').value = '0';
            }
            preferinteForm.submit();
        }
    </script>
    <script type="text/javascript">
        var preferinteForm = new varienForm('preferinteForm');
    </script>
    <?php } } ?>
</div>