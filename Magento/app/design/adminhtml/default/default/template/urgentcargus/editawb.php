<!--<script type="text/javascript" src="<?php echo Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB); ?>js/urgentcargus/urgentcargus.php"></script>-->
<div class="content-header">
    <h3>Urgent Cargus - Editare AWB comanda <?php echo addslashes($_GET['OrderId']) ?></h3>
</div>
<?php
$pickupPoints = Urgent_Cargus_Adminhtml_IndexController::getPickupPoints();
$judete = Urgent_Cargus_Adminhtml_IndexController::getCounties();
$data = Urgent_Cargus_Adminhtml_IndexController::getSingleAwbAsteptare();
?>
<div class="grid">
    <form id="editawbForm" name="editawbForm" method="post" action="<?php echo $this->getUrl('*/*/postEditawb'); ?>">
        <input name="form_key" type="hidden" value="<?php echo Mage::getSingleton('core/session')->getFormKey() ?>" />
        <input name="orderId" type="hidden" value="<?php echo addslashes($_GET['OrderId']) ?>" />
        <fieldset>
            <table cellspacing="0" class="data" width="100%">
                <tbody>
                    <tr class="headings">
                        <th colspan="2" class="no-link last"></th>
                    </tr>
                    <tr style="background:#eee;">
                        <td colspan="2"><strong>Expeditor</strong></td>
                    </tr>
                    <tr>
                        <td class="label" style="width:150px">Punctul de ridicare</td>
                        <td class="value last">
                            <select name="pickupLocationId" style="width:204px">
                                <?php
                                foreach ($pickupPoints as $pick) {
                                    echo '<option value="'.$pick['LocationId'].'" '.($data['pickupLocationId'] == $pick['LocationId'] ? 'selected="selected"' : '').'>'.$pick['Name'].'</option>';
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr class="headings">
                        <th colspan="2" class="no-link last"></th>
                    </tr>
                    <tr style="background:#eee">
                        <td colspan="2"><strong>Destinatar</strong></td>
                    </tr>
                    <tr>
                        <td class="label">Nume</td>
                        <td class="value last">
                            <input type="text" name="numeDest" value="<?php echo $data['numeDest']; ?>" maxlength="150" style="width:200px" />
                        </td>
                    </tr>
                    <tr>
                        <td class="label">Judet</td>
                        <td class="value last">
                            <select id="region_id" class="" name="judetDest" style="width:204px">
                                <?php
                                foreach ($judete as $jud) {
                                    echo '<option value="'.$jud['Abbreviation'].'" '.($data['judetDest'] == $jud['Abbreviation'] ? 'selected="selected"' : '').'>'.$jud['Name'].'</option>';
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">Localitate</td>
                        <td class="value last">
                            <input id="city" class="" type="text" name="localitateDest" value="<?php echo $data['localitateDest']; ?>" maxlength="150" style="width:200px" />
                        </td>
                    </tr>
                    <tr>
                        <td class="label">Adresa</td>
                        <td class="value last">
                            <input type="text" name="adresaDest" value="<?php echo $data['adresaDest']; ?>" maxlength="250" style="width:300px" />
                        </td>
                    </tr>
                    <tr>
                        <td class="label">Persoana contact</td>
                        <td class="value last">
                            <input type="text" name="contactDest" value="<?php echo $data['contactDest']; ?>" maxlength="150" style="width:200px" />
                        </td>
                    </tr>
                    <tr>
                        <td class="label">Telefon</td>
                        <td class="value last">
                            <input type="text" name="telefonDest" value="<?php echo $data['telefonDest']; ?>" maxlength="150" style="width:200px" />
                        </td>
                    </tr>
                    <tr>
                        <td class="label">Email</td>
                        <td class="value last">
                            <input type="text" name="emailDest" value="<?php echo $data['emailDest']; ?>" maxlength="150" style="width:200px" />
                        </td>
                    </tr>
                    <tr class="headings">
                        <th colspan="2" class="no-link last"></th>
                    </tr>
                    <tr style="background:#eee">
                        <td colspan="2"><strong>Detalii AWB</strong></td>
                    </tr>
                    <tr>
                        <td class="label">Plicuri</td>
                        <td class="value last">
                            <input type="text" name="plicuri" value="<?php echo $data['plicuri']; ?>" style="width:70px" />
                        </td>
                    </tr>
                    <tr>
                        <td class="label">Colete</td>
                        <td class="value last">
                            <input type="text" name="colete" value="<?php echo $data['colete']; ?>" style="width:70px" />
                        </td>
                    </tr>
                    <tr>
                        <td class="label">Greutate</td>
                        <td class="value last">
                            <input type="text" name="kilograme" value="<?php echo $data['kilograme']; ?>" style="width:50px" /> kg
                        </td>
                    </tr>
                    <tr>
                        <td class="label">Valoare declarata</td>
                        <td class="value last">
                            <input type="text" name="valoareDeclarata" value="<?php echo $data['valoareDeclarata']; ?>" style="width:70px" /> lei
                        </td>
                    </tr>
                    <tr>
                        <td class="label">Ramburs numerar</td>
                        <td class="value last">
                            <input type="text" name="rambursNumerar" value="<?php echo $data['rambursNumerar']; ?>" style="width:70px" /> lei
                        </td>
                    </tr>
                    <tr>
                        <td class="label">Ramburs cont colector</td>
                        <td class="value last">
                            <input type="text" name="rambursCont" value="<?php echo $data['rambursCont']; ?>" style="width:70px" /> lei
                        </td>
                    </tr>
                    <tr>
                        <td class="label">Ramburs alt tip</td>
                        <td class="value last">
                            <input type="text" name="rambursAlt" value="<?php echo $data['rambursAlt']; ?>" maxlength="250" style="width:200px" />
                        </td>
                    </tr>
                    <tr>
                        <td class="label">Platitor expeditie</td>
                        <td class="value last">
                            <select name="platitorExpeditie" style="width:204px">
                                <option value="1" <?php echo ($data['platitorExpeditie'] != 2 ? 'selected="selected"' : ''); ?>>Expeditor</option>
                                <option value="2" <?php echo ($data['platitorExpeditie'] == 2 ? 'selected="selected"' : ''); ?>>Destinatar</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">Livrare dimineata</td>
                        <td class="value last">
                            <select name="livrareDimineata" style="width:54px">
                                <option value="1" <?php echo ($data['livrareDimineata'] == 1 ? 'selected="selected"' : ''); ?>>Da</option>
                                <option value="0" <?php echo ($data['livrareDimineata'] != 1 ? 'selected="selected"' : ''); ?>>Nu</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">Livrare sambata</td>
                        <td class="value last">
                            <select name="livrareSambata" style="width:54px">
                                <option value="1" <?php echo ($data['livrareSambata'] == 1 ? 'selected="selected"' : ''); ?>>Da</option>
                                <option value="0" <?php echo ($data['livrareSambata'] != 1 ? 'selected="selected"' : ''); ?>>Nu</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">Deschidere colet</td>
                        <td class="value last">
                            <select name="deschidereColet" style="width:54px">
                                <option value="1" <?php echo ($data['deschidereColet'] == 1 ? 'selected="selected"' : ''); ?>>Da</option>
                                <option value="0" <?php echo ($data['deschidereColet'] != 1 ? 'selected="selected"' : ''); ?>>Nu</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">Observatii</td>
                        <td class="value last">
                            <input type="text" name="observatii" value="<?php echo $data['observatii']; ?>" maxlength="500" style="width:300px" />
                        </td>
                    </tr>
                    <tr>
                        <td class="label">Continut</td>
                        <td class="value last">
                            <input type="text" name="continut" value="<?php echo $data['continut']; ?>" maxlength="500" style="width:300px" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </fieldset>
    </form>
    <script type="text/javascript">
        var editawbForm = new varienForm('editawbForm');

        function cancelEdit() {
            window.location = '<?php echo $this->getUrl('*/*/index'); ?>';
        }
    </script>
    <style>
        select#city {
            width: 204px;
        }
    </style>
</div>
<button onclick="editawbForm.submit()" class="scalable save" type="button"><span>Salveaza modificari comanda <?php echo addslashes($_GET['OrderId']) ?></span></button>
<button onclick="cancelEdit()" class="scalable back" type="button"><span>Anuleaza</span></button>