<?php
$date = new DateTime();
$date->setTimezone(new DateTimeZone('Europe/Bucharest'));
$today = $date->format('Y-m-d H:i:s');
if (isset($_GET['date'])) {
    $d = explode('.', addslashes($_GET['date']));
    $date->setDate($d[2], $d[1], $d[0]);
}
$cd = $date->format('Y-m-d H:i:s');

if (date('w', strtotime($cd)) == 0) { // duminica
    $date = date('d.m.Y', strtotime($cd.' +1 day'));
    $h_start = 13;
    $h_end = 18;
    $h2_start = 14;
    $h2_end = 19;
} else if (date('w', strtotime($cd)) == 1 || date('w', strtotime($cd)) == 2 || date('w', strtotime($cd)) == 3 || date('w', strtotime($cd)) == 4) { // luni, marti, miercuri si joi
    if ($cd == $today) {
        if (date('H', strtotime($cd)) > 18) {
            $date = date('d.m.Y', strtotime($cd.' +1 day'));
            $h_start = 13;
            $h_end = 18;
            $h2_start = 14;
            $h2_end = 19;
        } else if (date('H', strtotime($cd)) == 18) {
            $date = date('d.m.Y', strtotime($cd));
            $h_start = 18;
            $h_end = 18;
            $h2_start = 19;
            $h2_end = 19;
        } else {
            $date = date('d.m.Y', strtotime($cd));
            $h_start = date('H', strtotime($cd)) + 1;
            $h_end = 18;
            $h2_start = date('H', strtotime($cd)) + 2;
            $h2_end = 19;
        }
    } else {
        $date = date('d.m.Y', strtotime($cd));
        $h_start = 13;
        $h_end = 18;
        $h2_start = 14;
        $h2_end = 19;
    }
} else if (date('w', strtotime($cd)) == 5) { // vineri
    if ($cd == $today) {
        if (date('H', strtotime($cd)) > 18) {
            $date = date('d.m.Y', strtotime($cd.' +1 day'));
            $h_start = 13;
            $h_end = 14;
            $h2_start = 14;
            $h2_end = 15;
        } else if (date('H', strtotime($cd)) == 18) {
            $date = date('d.m.Y', strtotime($cd));
            $h_start = 18;
            $h_end = 18;
            $h2_start = 19;
            $h2_end = 19;
        } else {
            $date = date('d.m.Y', strtotime($cd));
            $h_start = date('H', strtotime($cd)) + 1;
            $h_end = 18;
            $h2_start = date('H', strtotime($cd)) + 2;
            $h2_end = 19;
        }
    } else {
        $date = date('d.m.Y', strtotime($cd));
        $h_start = 13;
        $h_end = 18;
        $h2_start = 14;
        $h2_end = 19;
    }
} else if (date('w', strtotime($cd)) == 6) { // sambata
    if ($cd == $today) {
        if (date('H', strtotime($cd)) > 14) {
            $date = date('d.m.Y', strtotime($cd.' +2 day'));
            $h_start = 13;
            $h_end = 18;
            $h2_start = 14;
            $h2_end = 19;
        } else if (date('H', strtotime($cd)) == 14) {
            $date = date('d.m.Y', strtotime($cd));
            $h_start = 14;
            $h_end = 14;
            $h2_start = 15;
            $h2_end = 15;
        } else {
            $date = date('d.m.Y', strtotime($cd));
            $h_start = date('H', strtotime($cd)) + 1;
            $h_end = 14;
            $h2_start = date('H', strtotime($cd)) + 2;
            $h2_end = 15;
        }
    } else {
        $date = date('d.m.Y', strtotime($cd));
        $h_start = 13;
        $h_end = 14;
        $h2_start = 14;
        $h2_end = 15;
    }
}

if (isset($_GET['hour'])) {
    $h = explode(':', addslashes($_GET['hour']));
    $h2_start = $h[0] + 1;
    $hour = addslashes($_GET['hour']);
} else {
    $hour = false;
}

$h_dela = '';
for ($i = $h_start; $i <= $h_end; $i++) {
    $h_dela .= '<option'.($hour == $i.':00' ? ' selected="selected"' : '').'>'.$i.':00</option>';
}

$h_panala = '';
for ($i = $h2_start; $i <= $h2_end; $i++) {
    $h_panala .= '<option>'.$i.':00</option>';
}
?>

<style>
    .header, .notification-global, .footer {
        display: none !important;
    }
    html {
        background: #fff !important;
    }
    body {
        overflow: hidden;
    }
</style>

<div class="content-header">
    <h3>Urgent Cargus - Finalizare comanda</h3>
</div>
<h4>Va rugam sa alegeti intervalul de ridicare pentru comanda curenta</h4>

<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
jQuery(function() {
    jQuery('#datepicker').datepicker({
        minDate: 0,
        firstDay: 1,
        dateFormat: 'dd.mm.yy',
        beforeShowDay: function(date) {
            var day = date.getDay();
            return [(day != 0), ''];
        }
    });
    
    jQuery('#datepicker').change(function () {
        window.location = "?LocationId=<?php echo addslashes($_GET['LocationId']); ?>&date=" + jQuery(this).val();
    });
    
    jQuery('select[name="hour_from"]').change(function () {
        window.location = "?LocationId=<?php echo addslashes($_GET['LocationId']); ?>&date=" + jQuery('#datepicker').val() + "&hour=" + jQuery(this).val();
    });
});
</script>

<div class="entry-edit">
    <form id="sendorderForm" name="sendorderForm" method="post" action="<?php echo $this->getUrl('*/*/postSendorder'); ?>">
        <input name="form_key" type="hidden" value="<?php echo Mage::getSingleton('core/session')->getFormKey() ?>" />
        <input name="LocationId" type="hidden" value="<?php echo addslashes($_GET['LocationId']); ?>" />
        <input name="date" type="text" id="datepicker" value="<?php echo $date; ?>" />
        <select name="hour_from">
            <?php echo $h_dela; ?>
        </select>
        <select name="hour_to">
            <?php echo $h_panala; ?>
        </select>
        <button onclick="sendorderForm.submit()" class="scalable save" type="button"><span>Trimite comanda curenta</span></button>
    </form>
    <script type="text/javascript">
        var sendorderForm = new varienForm('sendorderForm');
    </script>
</div>