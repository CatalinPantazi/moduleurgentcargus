<?php
class Urgent_Cargus_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action {

    public function indexAction() {
        $token = null;
        $this->loadLayout()->renderLayout();
    }

    public function postIndexAsteptareAction($temp_awb_data = null) {
        if ($temp_awb_data == null) {
            $post = $this->getRequest()->getPost();
        } else {
            $post = array('items' => true);
        }
        if (isset($post['items'])) {
            $resource = Mage::getSingleton('core/resource');
            $read = $resource->getConnection('core_read');
            $write = $resource->getConnection('core_write');

            if ($temp_awb_data == null) {
                $sql = "SELECT * FROM `awb_expeditii` WHERE status = 0 AND orderId IN('" . implode("','", $post['items']) . "') ORDER BY timestamp DESC";
                $data = $read->fetchAll($sql);
            } else {
                $data = array(json_decode($temp_awb_data, true));
            }

            // initiez clasa urgent cargus
            require_once('app/code/local/Urgent/Cargus/Model/urgentcargus_class.php');
            $u = new UrgentCargusClass(Mage::getStoreConfig('carriers/urgent_cargus/urgent_apiurl'), Mage::getStoreConfig('carriers/urgent_cargus/urgent_apikey'));

            // UC login user
            $fields = array(
                'UserName' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_username'),
                'Password' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_password')
            );
            $token = $u->CallMethod('LoginUser', $fields, 'POST');

            // validez awb-urile din lista de asteptare
            $errors = array();
            $done = 0;
            foreach ($data as $item) {

                $fields = array(
                    'Sender' => array(
                        'LocationId' => $item['pickupLocationId']
                    ),
                    'Recipient' => array(
                        'LocationId' => null,
                        'Name' => $item['numeDest'],
                        'CountyId' => null,
                        'CountyName' => $item['judetDest'],
                        'LocalityId' => null,
                        'LocalityName' => $item['localitateDest'],
                        'StreetId' => null,
                        'StreetName' => '-',
                        'AddressText' => $item['adresaDest'],
                        'ContactPerson' => $item['contactDest'],
                        'PhoneNumber' => $item['telefonDest'],
                        'Email' => $item['emailDest'],
                        'CodPostal' => $item['postcodeDest']
                    ),
                    'Parcels' => $item['colete'],
                    'Envelopes' => $item['plicuri'],
                    'TotalWeight' => $item['kilograme'],
                    'DeclaredValue' => $item['valoareDeclarata'],
                    'CashRepayment' => $item['rambursNumerar'],
                    'BankRepayment' => $item['rambursCont'],
                    'OtherRepayment' => $item['rambursAlt'],
                    'OpenPackage' => $item['deschidereColet'] == 1 ? true : false,
                    'PriceTableId' => Mage::getStoreConfig('urgentcargus/price_id'),
                    'ShipmentPayer' => $item['platitorExpeditie'],
                    // 'ServiceId' => $item['platitorExpeditie'] == 1 ? 1 : 4,
                    'ServiceId' => $item['platitorExpeditie'] == 1 ? 34 : 4,
                    'MorningDelivery' => $item['livrareDimineata'] == 1 ? true : false,
                    'SaturdayDelivery' => $item['livrareSambata'] == 1 ? true : false,
                    'Observations' => $item['observatii'],
                    'PackageContent' => $item['continut'],
                    'CustomString' => $item['orderId']
                );
                $cod_bara = $u->CallMethod('Awbs', $fields, 'POST', $token);

                if (is_array($cod_bara)) {
                    if (isset($cod_bara['error'])) {
                        $errors[] = $item['orderId'].': '.$awb['error'];
                    }
                } else if ($cod_bara != '') {
                    $sql = "UPDATE `awb_expeditii` SET `codBara` = '".$cod_bara."', `status` = '1' WHERE `orderId` = '".$item['orderId']."'";
                    $write->query($sql);
                    ++$done;
                } else {
                    $errors[] = 'Unknown error!';
                }
            }
            if (count($data) == $done) {
                if ($temp_awb_data == null) {
                    Mage::getSingleton('adminhtml/session')->addSuccess('Toate AWB-urile bifate au fost validate!');
                } else {
                    return $cod_bara;
                }
            } elseif (count($data) == count($errors)) {
                Mage::getSingleton('adminhtml/session')->addError('Niciun AWB bifat nu a putut fi validat!<br/>'.implode('<br/>', $errors));
            } else {
                Mage::getSingleton('adminhtml/session')->addError(count($errors).' '.(count($errors) == 1 ? 'AWB' : 'AWB-uri').' '.(count($data) > 1 ? 'din cele '.count($data).' bifate' : '').' nu '.(count($errors) == 1 ? 'a' : 'au').' putut fi '.(count($errors) == 1 ? 'validat' : 'validate').'!<br/>'.implode('<br/>', $errors));
            }
        }
        if ($temp_awb_data == null) {
            $this->_redirect('*/*/index');
        }
    }

    public function postIndexAsteptareDeleteAction(){
        $post = $this->getRequest()->getPost();
        if (isset($post['items'])) {
            $resource = Mage::getSingleton('core/resource');
            $write = $resource->getConnection('core_write');

            $sql = "DELETE FROM `awb_expeditii` WHERE orderId IN('".implode("','", $post['items'])."')";
            $write->query($sql);

            Mage::getSingleton('adminhtml/session')->addSuccess('AWB-urile selectate au fost sterse!');
        }
        $this->_redirect('*/*/index');
    }

    public function postIndexValidateDeleteAction($barcode = null) {
        if ($barcode == null) {
            $post = $this->getRequest()->getPost();
        } else {
            $post = array('items' => array($barcode));
        }
        if (isset($post['items'])) {
            $resource = Mage::getSingleton('core/resource');
            $read = $resource->getConnection('core_read');
            $write = $resource->getConnection('core_write');

            // initiez clasa urgent cargus
            require_once('app/code/local/Urgent/Cargus/Model/urgentcargus_class.php');
            $u = new UrgentCargusClass(Mage::getStoreConfig('carriers/urgent_cargus/urgent_apiurl'), Mage::getStoreConfig('carriers/urgent_cargus/urgent_apikey'));

            // UC login user
            $fields = array(
                'UserName' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_username'),
                'Password' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_password')
            );
            $token = $u->CallMethod('LoginUser', $fields, 'POST');

            try {
                foreach ($post['items'] as $code) {
                    // sterg awb-urile validate din WebExpress
                    $result = $u->CallMethod('Awbs?barCode=' . $code, array(), 'DELETE', $token);
                }
                
                // sterg awb-urile selectate din lista cu validate
                if ($barcode == null) {
                    $sql = "UPDATE `awb_expeditii` SET `status` = '0', `codBara` = '' WHERE `codBara` IN('".implode("','", $post['items'])."')";
                } else {
                    $sql = "DELETE FROM `awb_expeditii` WHERE `codBara` IN('".implode("','", $post['items'])."')";
                }
                $write->query($sql);
                
                if ($barcode == null) {
                    Mage::getSingleton('adminhtml/session')->addSuccess('AWB-urile selectate au fost anulate!');
                } else {
                    return 'done';
                }
            } catch (Exception $ex) {
                Mage::getSingleton('adminhtml/session')->addError($ex->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function postIndexAwbprintAction() {
        if (isset($_GET['bar_codes'])) {
            // initiez clasa urgent cargus
            require_once('app/code/local/Urgent/Cargus/Model/urgentcargus_class.php');
            $u = new UrgentCargusClass(Mage::getStoreConfig('carriers/urgent_cargus/urgent_apiurl'), Mage::getStoreConfig('carriers/urgent_cargus/urgent_apikey'));

            // UC login user
            $fields = array(
                'UserName' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_username'),
                'Password' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_password')
            );
            $token = $u->CallMethod('LoginUser', $fields, 'POST');

            // UC print
            $print = $u->CallMethod('AwbDocuments?type=PDF&format=0&barCodes='.addslashes($_GET['bar_codes']), array(), 'GET', $token);

            header('Content-type:application/pdf');
            echo base64_decode($print);
            die();
        }
        $this->_redirect('*/*/index');
    }

    public function sendorderAction() {
        $token = null;
        $this->loadLayout()->renderLayout();
    }

    public function postSendorderAction() {
        $post = $this->getRequest()->getPost();

        // initiez clasa urgent cargus
        require_once('app/code/local/Urgent/Cargus/Model/urgentcargus_class.php');
        $u = new UrgentCargusClass(Mage::getStoreConfig('carriers/urgent_cargus/urgent_apiurl'), Mage::getStoreConfig('carriers/urgent_cargus/urgent_apikey'));

        // UC login user
        $fields = array(
            'UserName' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_username'),
            'Password' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_password')
        );
        $token = $u->CallMethod('LoginUser', $fields, 'POST');

        $d = explode('.', $post['date']);
        $from = $d[2].'-'.$d[1].'-'.$d[0].' '.$post['hour_from'].':00';
        $to = $d[2].'-'.$d[1].'-'.$d[0].' '.$post['hour_to'].':00';

        // UC send order
        $order_id = $u->CallMethod('Orders?locationId='.$post['LocationId'].'&PickupStartDate='.date('Y-m-d%20H:i:s', strtotime($from)).'&PickupEndDate='.date('Y-m-d%20H:i:s', strtotime($to)).'&action=1', array(), 'PUT', $token);

        echo '<script>window.opener.location.reload(); window.resizeTo(900, 600); window.location = "'.$this->getUrl('*/*/postPrintborderou').'?order_id='.$order_id.'";</script>';
        die();
    }

    public function postPrintborderouAction() {
        // initiez clasa urgent cargus
        require_once('app/code/local/Urgent/Cargus/Model/urgentcargus_class.php');
        $u = new UrgentCargusClass(Mage::getStoreConfig('carriers/urgent_cargus/urgent_apiurl'), Mage::getStoreConfig('carriers/urgent_cargus/urgent_apikey'));

        // UC login user
        $fields = array(
            'UserName' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_username'),
            'Password' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_password')
        );
        $token = $u->CallMethod('LoginUser', $fields, 'POST');

        // UC print borderou
        $borderou = $u->CallMethod('OrderDocuments?orderId='.addslashes($_GET['order_id']).'&docType=0', array(), 'GET', $token);

        header('Content-type:application/pdf');
        echo base64_decode($borderou);
        die();
    }

    public function editawbAction() {
        $token = null;
        $this->loadLayout()->renderLayout();
    }

    public function postEditawbAction() {
        $post = $this->getRequest()->getPost();

        $fields = array();
        foreach ($post as $key => $val) {
            if ($key == 'form_key' || $key == 'orderId') continue;
            $fields[] = "`".$key."` = '".addslashes($val)."'";
        }

        $resource = Mage::getSingleton('core/resource');
        $write = $resource->getConnection('core_write');

        $sql = "UPDATE `awb_expeditii` SET ".implode(', ', $fields)." WHERE `orderId` = '".$post['orderId']."'";
        $write->query($sql);

        Mage::getSingleton('adminhtml/session')->addSuccess('Modificarile au fost salvate!');
        $this->_redirect('*/*/index');
    }

    public function preferinteAction() {
        $token = null;
        $this->loadLayout()->renderLayout();
    }

    public function postPreferinteAction() {
        $post = $this->getRequest()->getPost();
        try {
            if (empty($post)) {
                Mage::throwException('Toate campurile sunt obligatorii!');
            }

            $resource = Mage::getSingleton('core/resource');
            $write = $resource->getConnection('core_write');

            $sql = "CREATE TABLE IF NOT EXISTS `awb_expeditii` (
                        `id` int(11) NOT NULL AUTO_INCREMENT,
                        `orderId` int(11) NOT NULL,
                        `pickupLocationId` int(11) NOT NULL,
                        `codBara` varchar(50) NOT NULL,
                        `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        `numeDest` varchar(150) NOT NULL,
                        `judetDest` varchar(150) NOT NULL,
                        `localitateDest` varchar(150) NOT NULL,
                        `adresaDest` varchar(250) NOT NULL,
                        `postcodeDest` varchar(64) NOT NULL,
                        `contactDest` varchar(150) NOT NULL,
                        `telefonDest` varchar(150) NOT NULL,
                        `emailDest` varchar(150) NOT NULL,
                        `plicuri` int(11) NOT NULL,
                        `colete` int(11) NOT NULL,
                        `kilograme` int(11) NOT NULL,
                        `valoareDeclarata` float(10,2) NOT NULL,
                        `rambursNumerar` float(10,2) NOT NULL,
                        `rambursCont` float(10,2) NOT NULL,
                        `rambursAlt` varchar(250) NOT NULL,
                        `platitorExpeditie` int(1) NOT NULL,
                        `livrareSambata` int(1) NOT NULL,
                        `livrareDimineata` int(1) NOT NULL,
                        `deschidereColet` int(1) NOT NULL,
                        `observatii` varchar(500) NOT NULL,
                        `continut` varchar(500) NOT NULL,
                        `status` int(1) NOT NULL,
                    PRIMARY KEY (`id`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";
            $write->query($sql);

            if (isset($_POST['update_nomenclator']) && $_POST['update_nomenclator'] == '1') {

                // adaug tabelele pentru judete si localitati
                $sql = "CREATE TABLE IF NOT EXISTS `awb_counties` (
                            `CountyId` int(11) unsigned NOT NULL AUTO_INCREMENT,
                            `Name` varchar(64) NOT NULL,
                            `Abbreviation` varchar(4) NOT NULL,
                            `CountryId` int(11) NOT NULL,
                        PRIMARY KEY (`CountyId`)
                        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";
                $write->query($sql);
                $sql = "CREATE TABLE IF NOT EXISTS `awb_localities` (
                            `LocalityId` int(11) unsigned NOT NULL AUTO_INCREMENT,
                            `Name` varchar(64) NOT NULL,
                            `ParentId` int(11) NOT NULL,
                            `ExtraKm` int(11) NOT NULL,
                            `InNetwork` tinyint(1) NOT NULL,
                            `CountyId` int(11) NOT NULL,
                            `CountryId` int(11) NOT NULL,
                        PRIMARY KEY (`LocalityId`)
                        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";
                $write->query($sql);

                // adaug judetele si localitatile in baza de date
                $regions = $this->getRegions();
                if (is_array($regions['counties'])) {
                    if (!isset($regions['counties']['error'])) {
                        $write->query("TRUNCATE TABLE awb_counties");
                        $sql = "INSERT INTO awb_counties (CountyId, Name, Abbreviation, CountryId) VALUES ";
                        $parts = array();
                        foreach ($regions['counties'] as $c) {
                            $parts[] = "('".$c['CountyId']."', '".$c['Name']."', '".$c['Abbreviation']."', '1')";
                        }
                        $sql .= implode(', ', $parts);
                        $write = $resource->getConnection('core_write');
                        $write->query($sql);
                    }
                }
                if (is_array($regions['localities'])) {
                    if (!isset($regions['localities']['error'])) {
                        $write->query("TRUNCATE TABLE awb_localities");
                        $sql = "INSERT INTO awb_localities (LocalityId, Name, ParentId, ExtraKm, InNetwork, CountyId, CountryId) VALUES ";
                        $parts = array();
                        foreach ($regions['localities'] as $l) {
                            $parts[] = "('".$l['LocalityId']."', '".$l['Name']."', '".$l['ParentId']."', '".$l['ExtraKm']."', '".$l['InNetwork']."', '".$l['CountyId']."', '".$l['CountryId']."')";
                        }
                        $sql .= implode(', ', $parts);
                        $write = $resource->getConnection('core_write');
                        $write->query($sql);
                    }
                }

                Mage::getSingleton('adminhtml/session')->addSuccess('Nomenclatorul a fost actualizat!');

            } else {
                $updateconfigdata = new Mage_Core_Model_Config();
                $updateconfigdata->saveConfig('urgentcargus/price_id', $post['price_id'], 'default', 0);
                $updateconfigdata->saveConfig('urgentcargus/pickup_id', $post['pickup_id'], 'default', 0);
                $updateconfigdata->saveConfig('urgentcargus/pickup_locality_id', $post['pickup_locality_id'], 'default', 0);
                $updateconfigdata->saveConfig('urgentcargus/openpackage', $post['openpackage'], 'default', 0);
                $updateconfigdata->saveConfig('urgentcargus/insurance', $post['insurance'], 'default', 0);
                $updateconfigdata->saveConfig('urgentcargus/saturday', $post['saturday'], 'default', 0);
                $updateconfigdata->saveConfig('urgentcargus/morning', $post['morning'], 'default', 0);
                $updateconfigdata->saveConfig('urgentcargus/rapayment', $post['rapayment'], 'default', 0);
                $updateconfigdata->saveConfig('urgentcargus/payer', $post['payer'], 'default', 0);
                $updateconfigdata->saveConfig('urgentcargus/nomenclator', $post['nomenclator'], 'default', 0);
                $updateconfigdata->saveConfig('urgentcargus/type', $post['type'], 'default', 0);
                $updateconfigdata->saveConfig('urgentcargus/plafon', $post['plafon'], 'default', 0);
                $updateconfigdata->saveConfig('urgentcargus/costfix', $post['costfix'], 'default', 0);

                Mage::getSingleton('adminhtml/session')->addSuccess('Preferintele au fost salvate!');
            }
        }
        catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        Mage::app()->getCacheInstance()->cleanType('config');
        $this->_redirect('*/*/preferinte');
    }

    public function istoricAction(){
        $token = null;
        $this->loadLayout()->renderLayout();
    }

    public static function checkCredentials() {
        //// initiez clasa urgent cargus
        //require_once('app/code/local/Urgent/Cargus/Model/urgentcargus_class.php');
        //$u = new UrgentCargusClass(Mage::getStoreConfig('carriers/urgent_cargus/urgent_apiurl'), Mage::getStoreConfig('carriers/urgent_cargus/urgent_apikey'));

        //// UC login user
        //$fields = array(
        //    'UserName' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_username'),
        //    'Password' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_password')
        //);
        //$token = $u->CallMethod('LoginUser', $fields, 'POST');

        return true;
    }

    public static function getPickupPoints() {
        // initiez clasa urgent cargus
        require_once('app/code/local/Urgent/Cargus/Model/urgentcargus_class.php');
        $u = new UrgentCargusClass(Mage::getStoreConfig('carriers/urgent_cargus/urgent_apiurl'), Mage::getStoreConfig('carriers/urgent_cargus/urgent_apikey'));

        // UC login user
        $fields = array(
            'UserName' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_username'),
            'Password' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_password')
        );
        $token = $u->CallMethod('LoginUser', $fields, 'POST');

        // obtin punctele de ridicare
        return $u->CallMethod('PickupLocations', array(), 'GET', $token);
    }

    public static function getPricePlans() {
        // initiez clasa urgent cargus
        require_once('app/code/local/Urgent/Cargus/Model/urgentcargus_class.php');
        $u = new UrgentCargusClass(Mage::getStoreConfig('carriers/urgent_cargus/urgent_apiurl'), Mage::getStoreConfig('carriers/urgent_cargus/urgent_apikey'));

        // UC login user
        $fields = array(
            'UserName' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_username'),
            'Password' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_password')
        );
        $token = $u->CallMethod('LoginUser', $fields, 'POST');

        // obtine lista planurilor tarifare
        return $u->CallMethod('PriceTables', array(), 'GET', $token);
    }

    public function getRegions() {
        // initiez clasa urgent cargus
        require_once('app/code/local/Urgent/Cargus/Model/urgentcargus_class.php');
        $u = new UrgentCargusClass(Mage::getStoreConfig('carriers/urgent_cargus/urgent_apiurl'), Mage::getStoreConfig('carriers/urgent_cargus/urgent_apikey'));

        // UC login user
        $fields = array(
            'UserName' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_username'),
            'Password' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_password')
        );
        $token = $u->CallMethod('LoginUser', $fields, 'POST');

        // obtin lista de judete
        $counties = $u->CallMethod('Counties?countryId=1', array(), 'GET', $token);

        // obtin lista de localitati
        $localities = array();
        foreach ($counties as $c) {
            $localities = array_merge($localities, $u->CallMethod('Localities?countryId=1&countyId='.$c['CountyId'], array(), 'GET', $token));
        }

        return array('counties' => $counties, 'localities' => $localities);
    }

    public static function getCounties() {
        // initiez clasa urgent cargus
        require_once('app/code/local/Urgent/Cargus/Model/urgentcargus_class.php');
        $u = new UrgentCargusClass(Mage::getStoreConfig('carriers/urgent_cargus/urgent_apiurl'), Mage::getStoreConfig('carriers/urgent_cargus/urgent_apikey'));

        // UC login user
        $fields = array(
            'UserName' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_username'),
            'Password' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_password')
        );
        $token = $u->CallMethod('LoginUser', $fields, 'POST');

        // obtin lista de judete din api
        return $u->CallMethod('Counties?countryId=1', array(), 'GET', $token);
    }

    public static function getOrderHistory($LocationId = null) {
        // initiez clasa urgent cargus
        require_once('app/code/local/Urgent/Cargus/Model/urgentcargus_class.php');
        $u = new UrgentCargusClass(Mage::getStoreConfig('carriers/urgent_cargus/urgent_apiurl'), Mage::getStoreConfig('carriers/urgent_cargus/urgent_apikey'));

        // UC login user
        $fields = array(
            'UserName' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_username'),
            'Password' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_password')
        );
        $token = $u->CallMethod('LoginUser', $fields, 'POST');

        // UC get istoric comenzi
        return $u->CallMethod('Orders?locationId='.($LocationId == null ? Mage::getStoreConfig('urgentcargus/pickup_id') : $LocationId).'&status=1&pageNumber=1&itemsPerPage=100', array(), 'GET', $token);
    }

    public static function getAwbsByOrderId($OrderId) {
        // initiez clasa urgent cargus
        require_once('app/code/local/Urgent/Cargus/Model/urgentcargus_class.php');
        $u = new UrgentCargusClass(Mage::getStoreConfig('carriers/urgent_cargus/urgent_apiurl'), Mage::getStoreConfig('carriers/urgent_cargus/urgent_apikey'));

        // UC login user
        $fields = array(
            'UserName' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_username'),
            'Password' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_password')
        );
        $token = $u->CallMethod('LoginUser', $fields, 'POST');

        // UC get istoric awb-uri comanda
        return $u->CallMethod('Awbs?&orderId=' . $OrderId, array(), 'GET', $token);
    }

    public static function getAwbByBarcode($BarCode) {
        // initiez clasa urgent cargus
        require_once('app/code/local/Urgent/Cargus/Model/urgentcargus_class.php');
        $u = new UrgentCargusClass(Mage::getStoreConfig('carriers/urgent_cargus/urgent_apiurl'), Mage::getStoreConfig('carriers/urgent_cargus/urgent_apikey'));

        // UC login user
        $fields = array(
            'UserName' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_username'),
            'Password' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_password')
        );
        $token = $u->CallMethod('LoginUser', $fields, 'POST');

        // UC get detalii awb
        return $u->CallMethod('Awbs?&barCode=' . $BarCode, array(), 'GET', $token);
    }

    public static function getAwbsAsteptare() {
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');

        $sql = "SELECT * FROM `awb_expeditii` WHERE status = 0 ORDER BY timestamp DESC";
        return $read->fetchAll($sql);
    }

    public static function getSingleAwbAsteptare($OrderId) {
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');

        $sql = "SELECT * FROM `awb_expeditii` WHERE status = 0 AND orderId = '".addslashes($_GET['OrderId'])."'";
        $result = $read->fetchAll($sql);
        return $result[0];
    }

    public static function getCurrentOrder($LocationId = null) {
        // initiez clasa urgent cargus
        require_once('app/code/local/Urgent/Cargus/Model/urgentcargus_class.php');
        $u = new UrgentCargusClass(Mage::getStoreConfig('carriers/urgent_cargus/urgent_apiurl'), Mage::getStoreConfig('carriers/urgent_cargus/urgent_apikey'));

        // UC login user
        $fields = array(
            'UserName' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_username'),
            'Password' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_password')
        );
        $token = $u->CallMethod('LoginUser', $fields, 'POST');

        // UC get comanda curenta
        $orders = $u->CallMethod('Orders?locationId='.($LocationId == null ? Mage::getStoreConfig('urgentcargus/pickup_id') : $LocationId).'&status=0&pageNumber=1&itemsPerPage=1000', array(), 'GET', $token);

        // UC get awb-uri curente
        $awb = array();
        if (!is_null($orders)) {
            $result = $u->CallMethod('Awbs?&orderId='.(isset($orders['OrderId']) == 1 ? $orders['OrderId'] : $orders[0]['OrderId']), array(), 'GET', $token);
            if (!is_null($result)) {
                foreach ($result as $t) {
                    if ($t['Status'] != 'Deleted') {
                        $awb[] = $t;
                    }
                }
            }
        }
        return $awb;
    }
}