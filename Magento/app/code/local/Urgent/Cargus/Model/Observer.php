<?php
class Urgent_Cargus_Model_Observer extends Mage_Core_Model_Abstract {
	public function createAwb($observer) {
		// adaug awb-ul atunci cand este creat un shipment pe baza comenzii din magento

		$shipment = $observer->getEvent()->getShipment();

		$tracknums = array();
		foreach ($shipment->getAllTracks() as $tracknum) {
			$tracknums[] = $tracknum->getNumber();
		}

		// daca exista deja un awb pentru aceasta comanda nu mai adaug altul
		if (count($tracknums) == 0) {

			$order = $shipment->getOrder();
			//var_dump($order); die();

			if ($order->getShippingMethod() == 'urgent_cargus_urgent_cargus') {

				$order_id = $order->getId();
				$order_no = $order->getIncrementId();

				$curl = curl_init();
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
				curl_setopt($curl, CURLOPT_TIMEOUT, 10);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
				curl_setopt($curl, CURLOPT_URL, Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB, true) . 'urgentcargus/AddUrgent.php?id=' . $order_id . '&id_long=' . $order_no . '&timestamp=' . time() . '&standalone=1');
				$curl_result = curl_exec($curl);

				function isJson($string) {
					json_decode($string);
					return (json_last_error() == JSON_ERROR_NONE);
				}

				if (isJson($curl_result)) {

					require_once 'app/code/local/Urgent/Cargus/controllers/Adminhtml/IndexController.php';
					$controller = Mage::getControllerInstance(
						'Urgent_Cargus_Adminhtml_IndexController',
						Mage::app()->getRequest(),
						Mage::app()->getResponse());
					$barcode = $controller->postIndexAsteptareAction($curl_result);

					if ($barcode) {
						$tracking = new Mage_Sales_Model_Order_Shipment_Track();
						$tracking->setOrderId($shipment->getOrderId());
						$tracking->setCarrierCode('urgent_cargus');
						$tracking->setNumber($barcode);
						$tracking->setTitle('AWB');
						$shipment->addTrack($tracking);

						$print = Mage::helper('adminhtml')->getUrl('urgentcargus/adminhtml_index/postIndexAwbprint').'?bar_codes=['.$barcode.']';
						$print = str_replace(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB, true), '', $print);
						$print = '/'.trim($print, '/');
						$javascript = '<script>window.open("'.$print.'", "", "width=900, height=600, left=50, top=50");</script>';

						$button = '<button type="button" class="scalable" onclick="window.open(\''.$print.'\', \'\', \'width=900, height=600, left=50, top=50\')" style="margin-left:20px;"><span>Printeaza AWB '.$barcode.'</span></button>';

						Mage::getSingleton('adminhtml/session')->addSuccess('AWB-ul UrgentCargus ' . $barcode . ' a fost adaugat cu succes!'.$button.$javascript);
					}

				} else {
					// raspunsul primit dupa crearea awb-ului in asteptare nu este un json, asa ca afisez o eroare
					switch ($curl_result) {
					case 'old':
						Mage::getSingleton('core/session')->addError('Aceasta comanda a fost deja trimisa sau adaugata in lista de asteptare Urgent Cargus! NU A FOST CREAT UN NOU AWB!');
						break;
					default:
						Mage::getSingleton('core/session')->addError($curl_result);
						break;
					}
				}

			} else {
				// curierul nu este Urgent Cargus, asa ca doar ii atasez un mesaj de eroare
				Mage::getSingleton('core/session')->addError('Comanda nu are livrare cu Urgent Cargus!');
			}
		}
	}

	public function deleteAwb($observer) {
		$track = $observer->getEvent()->getTrack();
		if ($track->getCarrierCode() == 'urgent_cargus') {
			$barcode = $track->getNumber();

			require_once 'app/code/local/Urgent/Cargus/controllers/Adminhtml/IndexController.php';
			$controller = Mage::getControllerInstance(
				'Urgent_Cargus_Adminhtml_IndexController',
				Mage::app()->getRequest(),
				Mage::app()->getResponse());
			$re = $controller->postIndexValidateDeleteAction($barcode);
		}
	}

    public function addTracking($observer) {
        $block = $observer->getEvent()->getBlock();
        if ($block instanceof Mage_Shipping_Block_Tracking_Popup)
        {
            //$block->getLayout()->getBlock('head')->addJs('test.js');

            $ship_id = Mage::registry('current_shipping_info')->ship_id;
            $order_id = Mage::registry('current_shipping_info')->order_id;

            $tracking_info = array();

            if ($ship_id) {
                $shipment = Mage::getModel('sales/order_shipment')->load($ship_id);
                $tracking_info = array_merge($tracking_info, $shipment->getAllTracks());
                $order = $shipment->getOrder();
            }

            if ($order_id) {
                $order = Mage::getModel('sales/order')->load($order_id);
                $shipments = $order->getShipmentsCollection();
                foreach ($shipments as $shipment) {
                    $tracking_info = array_merge($tracking_info, $shipment->getAllTracks());
                }
            }

            if ($order->getShippingMethod() == 'urgent_cargus_urgent_cargus') {
                foreach ($tracking_info as $track) {
                    if ($track->getCarrierCode() == 'urgent_cargus') {
                        $barcode = $track->getNumber();
                        //$barcode = 829406131;

                        if ($barcode) {

                            require_once('app/code/local/Urgent/Cargus/Model/urgentcargus_class.php');
                            $u = new UrgentCargusClass(Mage::getStoreConfig('carriers/urgent_cargus/urgent_apiurl'), Mage::getStoreConfig('carriers/urgent_cargus/urgent_apikey'));

                            $fields = array(
                                'UserName' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_username'),
                                'Password' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_password')
                            );
                            $token = $u->CallMethod('LoginUser', $fields, 'POST');

                            $trace = $u->CallMethod('AwbTrace?barCode=[' . $barcode . ']', null, 'GET', $token);

                            foreach ($trace as $t) {
	                            echo '<div class="grid">
	                            	<table>
	                            		<tr class="headings">
	                            			<td colspan="3">AWB '.$t['Code'].'</td>
	                            		</tr>';
	                            		if (count($t['Event']) == 0) {
                            				echo '<tr><td colspan="3">Nicio informatie disponibila!</td></tr>';
	                            		} else {
		                            		foreach ($t['Event'] as $e) {
		                            			echo '<tr><td>'.date('d.m.Y H:i', strtotime($e['Date'])).'</td><td>'.$e['LocalityName'].'</td><td>'.$e['Description'].'</td></tr>';
		                            		}
		                            	}
	                            	echo '</table>
	                            </div>';
	                            //var_dump($t);
                    		}

                    		echo '<style>
	                    		table {
	                    			width: 100%;
									border-collapse: collapse;
									font: 12px Helvetica, sans-serif;
								}
								td {
									padding: 8px;
									border: 1px solid #ccc;
								}
								tr:nth-child(odd) td {
									background: #fafafa;
								}
								tr.headings td {
									background: #ffac47;
									color: #fff;
									border-color: #ed6502 #a04300 #a04300 #ed6502;
								}
                    		</style>';

                            die();

                        }
                    }
                }
            }
        }
    }
}
?>