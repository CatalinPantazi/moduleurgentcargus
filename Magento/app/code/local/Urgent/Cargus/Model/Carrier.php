<?php
class Urgent_Cargus_Model_Carrier extends Mage_Shipping_Model_Carrier_Abstract implements Mage_Shipping_Model_Carrier_Interface {
    protected $_code = 'urgent_cargus';
    public function collectRates(Mage_Shipping_Model_Rate_Request $request) {
        $result = Mage::getModel('shipping/rate_result');
        if (Mage::getStoreConfig('carriers/'.$this->_code.'/active')) {
            $result->append($this->_calculeazaTransport($request));
        }
        return $result;
    }
    public function getAllowedMethods() {
        return array(
            'urgent' => 'Urgent Cargus'
        );
    }
    public function _calculeazaTransport(Mage_Shipping_Model_Rate_Request $request) {
        $method = Mage::getModel('shipping/rate_result_method');
        $method->setCarrier($this->_code);
        $method->setCarrierTitle('Urgent Cargus');
        $method->setMethod($this->_code);
        $method->setMethodTitle('Standard');

        if (strlen(trim($request->getDestCity())) < 1 || strlen(trim($request->getDestRegionCode())) < 1) {
            return false;
        }

        // calculeaza cursul de schimb valutar
        $base2ron = 1;
        $ron2base = 1;
        $baseCode = Mage::app()->getBaseCurrencyCode();
        if (strtolower($baseCode) != 'ron') {
            $allowedCurrencies = Mage::getModel('directory/currency')->getConfigAllowCurrencies();
            $baseRates = Mage::getModel('directory/currency')->getCurrencyRates($baseCode, array_values($allowedCurrencies));
            $rates = array();
            foreach ($baseRates as $k => $v) {
                $rates[strtolower($k)] = $v;
            }
            if (!isset($rates['ron'])) {
                return false;
            }
            $base2ron = $rates['ron'] / $rates[strtolower($baseCode)];
            $ron2base = $rates[strtolower($baseCode)] / $rates['ron'];
        }

        // elimina produsele care nu necesita transport
        if ($request->getAllItems()) {
            foreach ($request->getAllItems() as $item) {
                if ($item->getParentItem()) {
                    continue;
                }
                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    foreach ($item->getChildren() as $child) {
                        if ($child->getProduct()->isVirtual()) {
                            $request->setPackageValue($request->getPackageValue() - $child->getBaseRowTotal());
                        }
                    }
                } elseif ($item->getProduct()->isVirtual()) {
                    $request->setPackageValue($request->getPackageValue() - $item->getBaseRowTotal());
                }
            }
        }

        // obtine totalul cosului din request
        $valoare = $request->getPackageValue() * $base2ron;

        // stabilesc daca se ofera transport gratuit
        if (Mage::getStoreConfig('urgentcargus/plafon') > 0 && $valoare > Mage::getStoreConfig('urgentcargus/plafon')) {
            $method->setCost(0);
            $method->setPrice(0);
            return $method;
        }

        // stabilesc daca exista un cost fix configurat si setez pretul in functie de plafonul pentru transport gratuit
        if (Mage::getStoreConfig('urgentcargus/costfix') || Mage::getStoreConfig('urgentcargus/costfix') == '0') {
            $costfix = Mage::getStoreConfig('urgentcargus/costfix');
            $method->setCost($costfix);
            $method->setPrice($costfix);
            return $method;
        }

        // calculeaza transportul
        if (Mage::getStoreConfig('urgentcargus/insurance') == '1') {
            $DeclaredValue = round($valoare, 2);
        } else {
            $DeclaredValue = 0;
        }
        if (Mage::getStoreConfig('urgentcargus/rapayment') == 'bank') {
            $CashRepayment = 0;
            $BankRepayment = round($valoare, 2);
        } else {
            $CashRepayment = round($valoare, 2);
            $BankRepayment = 0;
        }
        if (isset($_POST['payment']['method'])) {
            if (strpos($_POST['payment']['method'], 'cashondelivery') !== false) {} else {
                $CashRepayment = 0;
                $BankRepayment = 0;
            }
        }

        // initiez clasa urgent cargus
        require_once('app/code/local/Urgent/Cargus/Model/urgentcargus_class.php');
        $u = new UrgentCargusClass(Mage::getStoreConfig('carriers/urgent_cargus/urgent_apiurl'), Mage::getStoreConfig('carriers/urgent_cargus/urgent_apikey'));

        // UC login user
        $fields = array(
            'UserName' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_username'),
            'Password' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_password')
        );
        $token = $u->CallMethod('LoginUser', $fields, 'POST');

        // UC punctul de ridicare default
        $location = array();
        $pickups = $u->CallMethod('PickupLocations', array(), 'GET', $token);
        if (is_null($pickups)) {
            die('Nu exista niciun punct de ridicare asociat acestui cont!');
        }
        foreach ($pickups as $pick) {
            if (Mage::getStoreConfig('urgentcargus/pickup_id') == $pick['LocationId']) {
                $location = $pick;
            }
        }

        $weight = ceil($request->getPackageWeight());

        if ($weight > 1) {
            $no_envelopes = 0;
            $no_parcels = 1;
        } else {
            $no_envelopes = (Mage::getStoreConfig('urgentcargus/type') == 'envelope' ? 1 : 0);
            $no_parcels = (Mage::getStoreConfig('urgentcargus/type') != 'envelope' ? 1 : 0);
        }

        // UC shipping calculation
        $fields = array(
            'FromLocalityId' => $location['LocalityId'],
            'ToLocalityId' => 0,
            'FromCountyName' => '',
            'FromLocalityName' => '',
            'ToCountyName' => $request->getDestRegionCode(),
            'ToLocalityName' => $request->getDestCity(),
            'Parcels' => $no_parcels,
            'Envelopes' => $no_envelopes,
            'TotalWeight' => $weight,
            'DeclaredValue' => $DeclaredValue,
            'CashRepayment' => $CashRepayment,
            'BankRepayment' => $BankRepayment,
            'OtherRepayment' => '',
            'PaymentInstrumentId' => 0,
            'PaymentInstrumentValue' => 0,
            'OpenPackage' => (Mage::getStoreConfig('urgentcargus/openpackage') != 1 ? false : true),
            'SaturdayDelivery' => (Mage::getStoreConfig('urgentcargus/saturday') != 1 ? false : true),
            'MorningDelivery' => (Mage::getStoreConfig('urgentcargus/morning') != 1 ? false : true),
            'ShipmentPayer' => (Mage::getStoreConfig('urgentcargus/payer') != 'recipient' ? 1 : 2),
            // 'ServiceId' => (Mage::getStoreConfig('urgentcargus/payer') != 'recipient' ? 1 : 4),
            'ServiceId' => (Mage::getStoreConfig('urgentcargus/payer') != 'recipient' ? 34 : 4),
            'PriceTableId' => Mage::getStoreConfig('urgentcargus/price_id')
        );
        $calculate = $u->CallMethod('ShippingCalculation', $fields, 'POST', $token);

        if (is_null($calculate) || (is_array($calculate) && isset($calculate['Error']))) {
            /*echo '<pre>';
            print_r($calculate);
            print_r($fields);
            die();*/
            return false;
        }

        $total = round($calculate['GrandTotal'] * $ron2base, 2);
        if ($request->getFreeShipping()) {
            $method->setCost(0);
            $method->setPrice(0);
        } else {
            $method->setCost($total);
            $method->setPrice($total);
        }
        return $method;
    }
    public function isTrackingAvailable() {
        return true;
    }
}