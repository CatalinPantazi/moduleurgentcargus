﻿jQuery(function () {
	// adauga buton submit in head-ul orders
	jQuery('.adminhtml-sales-order-index .content-header .form-buttons')
		.prepend('<button id="add_urgent_bulk" type="button" class="scalable add"><span>Adauga in lista de livrari Urgent Cargus</span></button>');

	// adauga buton submit in head-ul unei comenzi individuale
	jQuery('.adminhtml-sales-order-view .content-header .form-buttons')
		.prepend('<button id="add_urgent_single" type="button" class="scalable add"><span>Adauga in lista de livrari Urgent Cargus</span></button>');

	// ruleaza ajax in loop pt adaugarea comenzilor selectate in lista de livrare
	jQuery('#add_urgent_bulk').live('click', function () {
		var status_final = '';
		var status_final_1 = '';
		var status_final_2 = '';
		var status_final_3 = '';
		jQuery('input[name="order_ids"]:checked').each(function () {
		    var id = parseInt(jQuery(this).val());
		    var tempid = jQuery(this).parent('td').next('td').text().replace('-', '').match(/\d+/)[0];
		    var id_long = parseInt(tempid);
			jQuery.ajax({
				async: false,
				url: '../../../../../../urgentcargus/AddUrgent.php?id=' + id + '&id_long=' + id_long + '&timestamp=' + new Date().getTime(),
				success: function (data) {
					if (data == 'ok') status_final_1 = status_final_1 + " - " + id_long + "\n";
					else if (data == 'old') status_final_2 = status_final_2 + " - " + id_long + "\n";
					else status_final_3 = status_final_3 + " - " + id_long + "\n";
				}
			});
		});
		if (status_final_1) status_final = status_final + "Comenzile de mai jos au fost adaugate cu succes in lista pentru livrare:\n" + status_final_1;
		if (status_final_2) status_final = status_final + "Comenzile de mai jos au fost livrate sau sunt deja in lista pentru livrare:\n" + status_final_2;
		if (status_final_3) status_final = status_final + "Comenzile de mai jos nu au putut fi adaugate in lista pentru livrare:\n" + status_final_3;
		if (status_final) alert(status_final); else alert('Nu ati selectat nicio comanda pentru a fi adaugata in lista de livrari Urgent Cargus!');
		return false;
	});

	// ruleaza ajax pentru adaugarea unei comenzi individuale in lista de livrare
	jQuery('#add_urgent_single').live('click', function () {
		var urlq = document.URL;
		var urlvar = urlq.split('/');
		var single_order_id = urlvar[8];

		var soil = jQuery('.adminhtml-sales-order-view .content-header .head-sales-order').html();
		var soil = soil.replace('Order # ', '');
		var soil2 = soil.split('|');
		var tempid = soil2[0].replace('-', '').match(/\d+/)[0];
		var single_order_id_long = parseInt(tempid);
		jQuery.ajax({
			async: false,
			url: '../../../../../../../../urgentcargus/AddUrgent.php?id=' + single_order_id + '&id_long=' + single_order_id_long + '&timestamp=' + new Date().getTime(),
			success: function (data) {
				if (data == 'ok') alert('Comanda curenta a fost adaugata cu succes in lista pentru livrare.');
				else if (data == 'old') alert('Comanda curenta a fost livrata sau este deja in lista pentru livrare.');
				else alert('Comanda curenta nu a putut fi adaugata in lista pentru livrare.');
			}
		});
		return false;
	});
});