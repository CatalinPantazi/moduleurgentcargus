<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once(realpath(dirname(__FILE__)).'/../app/Mage.php');

$idclient = Mage::getStoreConfig('carriers/urgent_cargus/urgent_username');
$resource = Mage::getSingleton('core/resource');
$dbo_read = $resource->getConnection('core_read');
$dbo_write = $resource->getConnection('core_write');

$orderId = intval(addslashes($_GET['id']));
$orderName = intval(addslashes($_GET['id_long']));

$exs = $dbo_read->fetchAll("SELECT `id` FROM `awb_expeditii` WHERE `orderId`='".$orderName."'");
if (is_array($exs[0])) die('old');

function c($var) {
    return addslashes(trim($var));
}

$sel = "SELECT
            sfo.weight,
            sfo.order_currency_code,
            sfo.created_at,
            sfo.grand_total,
            sfo.shipping_amount,
            sfo.shipping_tax_amount,
            sfo.total_paid,
            sfoa.region,
            sfoa.region_id,
            sfoa.city,
            sfoa.street,
            sfoa.postcode,
            sfoa.telephone,
            sfoa.lastname,
            sfoa.firstname,
            sfoa.middlename,
            sfoa.company,
            sfoa.email,
            sfop.method,
            GROUP_CONCAT(sfoi.name SEPARATOR '; ') AS continut
        FROM
            ".$resource->getTableName('sales_flat_order')." sfo
        LEFT JOIN ".$resource->getTableName('sales_flat_order_address')." sfoa ON sfo.shipping_address_id = sfoa.entity_id
        LEFT JOIN ".$resource->getTableName('sales_flat_order_item')." sfoi ON sfo.entity_id = sfoi.order_id
        LEFT JOIN ".$resource->getTableName('sales_flat_order_payment')." sfop ON sfo.entity_id = sfop.parent_id
        WHERE
            sfo.entity_id = ".$orderId." AND
            sfoi.parent_item_id IS NULL AND
            sfoa.address_type = 'shipping'
        GROUP BY sfo.increment_id
        LIMIT 0,1";
$result = $dbo_read->fetchAll($sel);
$data = $result[0];

// verific daca moneda in care a fost plasata comanda este RON si stabilesc cursul valutar in caz contrar
$base2ron = 1;
$ron2base = 1;
$baseCode = $data['order_currency_code'];
if ($baseCode != 'RON') {
    $defaultCurrency = Mage::app()->getStore()->getCurrentCurrencyCode();
    $allowedCurrencies = Mage::getModel('directory/currency')->getConfigAllowCurrencies();
    $rates = Mage::getModel('directory/currency')->getCurrencyRates($defaultCurrency, array_values($allowedCurrencies));
    $base2ron = $rates['RON'] / $rates[$baseCode];
    $ron2base = $rates[$baseCode] / $rates['RON'];
}
if (Mage::getStoreConfig('urgentcargus/payer') != 'recipient' || (Mage::getStoreConfig('urgentcargus/costfix') || Mage::getStoreConfig('urgentcargus/costfix') == '0')) {
    $payer = 1;
    $valoareRamburs = round($data['grand_total'] * $base2ron, 2);
} else {
    $payer = 2;
    $valoareRamburs = round(($data['grand_total'] - $data['shipping_amount'] - $data['shipping_tax_amount']) * $base2ron, 2);
}

// daca transportul este gratuit, plata o sa fie la expeditor
if ($data['shipping_amount'] == 0) {
    $payer = 1;
}

if (Mage::getStoreConfig('urgentcargus/insurance') == '1') {
    $DeclaredValue = round(($data['grand_total'] - $data['shipping_amount']) * $base2ron, 2);
} else {
    $DeclaredValue = 0;
}
if (strpos($data['method'], 'cashondelivery') !== false) {
    if (Mage::getStoreConfig('urgentcargus/rapayment') == 'bank') {
        $CashRepayment = 0;
        $BankRepayment = round($valoareRamburs, 2);
    } else {
        $CashRepayment = round($valoareRamburs, 2);
        $BankRepayment = 0;
    }
} else {
    $CashRepayment = 0;
    $BankRepayment = 0;
}

// preiau indicativul judetului, dupa denumire
$judet = "SELECT * FROM ".$resource->getTableName('directory_country_region')." WHERE `country_id` = 'RO' AND `region_id` = '".$data['region_id']."'";
$judetResult = $dbo_read->fetchAll($judet);

$weight = ($data['weight'] < 1 ? 1 : ceil($data['weight']));

if ($weight > 1) {
    $no_envelopes = 0;
    $no_parcels = 1;
} else {
    $no_envelopes = (Mage::getStoreConfig('urgentcargus/type') == 'envelope' ? 1 : 0);
    $no_parcels = (Mage::getStoreConfig('urgentcargus/type') != 'envelope' ? 1 : 0);
}

$sql = "INSERT INTO `awb_expeditii` (
                            `orderId`,
                            `pickupLocationId`,
                            `codBara`,
                            `numeDest`,
                            `judetDest`,
                            `localitateDest`,
                            `adresaDest`,
                            `postcodeDest`,
                            `contactDest`,
                            `telefonDest`,
                            `emailDest`,
                            `plicuri`,
                            `colete`,
                            `kilograme`,
                            `valoareDeclarata`,
                            `rambursNumerar`,
                            `rambursCont`,
                            `platitorExpeditie`,
                            `livrareSambata`,
                            `livrareDimineata`,
                            `deschidereColet`,
                            `observatii`,
                            `continut`,
                            `status`
                        ) VALUES (
                            '".c($orderName)."',
                            '".Mage::getStoreConfig('urgentcargus/pickup_id')."',
                            '',
                            '".(c($data['company']) ? c($data['company']) : c($data['firstname']).' '.c($data['lastname']))."',
                            '".c($judetResult[0]['code'])."',
                            '".c($data['city'])."',
                            '".c($data['street'])."',
                            '".c($data['postcode'])."',
                            '".c($data['firstname']).' '.c($data['lastname'])."',
                            '".c($data['telephone'])."',
                            '".c($data['email'])."',
                            '".$no_envelopes."',
                            '".$no_parcels."',
                            '".$weight."',
                            '".$DeclaredValue."',
                            '".$CashRepayment."',
                            '".$BankRepayment."',
                            '".$payer."',
                            '".Mage::getStoreConfig('urgentcargus/saturday')."',
                            '".Mage::getStoreConfig('urgentcargus/morning')."',
                            '".Mage::getStoreConfig('urgentcargus/openpackage')."',
                            '',
                            '".c($data['continut'])."',
                            '0'
                        );";

$dbo_write->query($sql);

if (!isset($_GET['standalone'])) {
    echo 'ok';
} else {
    $out = $dbo_read->fetchAll("SELECT * FROM `awb_expeditii` WHERE `orderId` = '".c($orderName)."' LIMIT 1");
    if (count($out) > 0) {
        echo json_encode($out[0]);
    } else {
        echo 'bad';
    }
}
?>