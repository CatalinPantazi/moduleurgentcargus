<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

if (isset($_GET['id'])) {
    $get_judet = trim(addslashes($_GET['id']));
    if ($get_judet == '') {
        echo 'null';
        die();
    } else {
        if (is_numeric($get_judet)) {
            $id_judet = $get_judet;
        } else {
            $judet['code'] = $get_judet;
        }
    }
} else {
    echo 'null';
    die();
}

if (isset($_GET['val'])) {
    $val = addslashes($_GET['val']);
} else {
    $val = '';
}

require_once(realpath(dirname(__FILE__)).'/../app/Mage.php');

$idclient = Mage::getStoreConfig('carriers/urgent_cargus/urgent_username');
$resource = Mage::getSingleton('core/resource');
$read = $resource->getConnection('core_read');

if (isset($id_judet)) {
    $sql = "SELECT * FROM ".$resource->getTableName('directory_country_region')." WHERE country_id = 'RO' AND region_id = '".$id_judet."'";
    $result = $read->fetchAll($sql);
    if (is_array($result) && count($result) == 1) {
        $judet = $result[0];
    } else {
        echo 'null';
        die();
    }
}

if (Mage::getStoreConfig('urgentcargus/nomenclator') == 'local') {

    $localities = $read->fetchAll("SELECT l.Name, l.InNetwork, l.ExtraKm FROM awb_localities l LEFT JOIN awb_counties c ON c.CountyId = l.CountyId WHERE c.Abbreviation = '".$judet['code']."' ORDER BY l.Name ASC");
    echo '<option value="" rel="">---</option>'."\n";
    foreach ($localities as $row) {
        echo '<option '.(trim(strtolower($val)) == trim(strtolower($row['Name'])) ? 'selected="selected"' : '').' km="'.($row['InNetwork'] ? 0 : $row['ExtraKm']).'">'.$row['Name'].'</option>'."\n";
    }

} else {

    require_once(realpath(dirname(__FILE__)).'/../app/Mage.php');

    // initiez clasa urgent cargus
    require_once(realpath(dirname(__FILE__)).'/../app/code/local/Urgent/Cargus/Model/urgentcargus_class.php');
    $u = new UrgentCargusClass(Mage::getStoreConfig('carriers/urgent_cargus/urgent_apiurl'), Mage::getStoreConfig('carriers/urgent_cargus/urgent_apikey'));

    // UC login user
    $fields = array(
        'UserName' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_username'),
        'Password' => Mage::getStoreConfig('carriers/urgent_cargus/urgent_password')
    );
    $token = $u->CallMethod('LoginUser', $fields, 'POST');

    // obtin lista de judete din api
    $judete = array();
    $dataJudete = $u->CallMethod('Counties?countryId=1', array(), 'GET', $token);
    foreach ($dataJudete as $j) {
        $judete[strtolower($j['Abbreviation'])] = $j['CountyId'];
    }

    // obtin lista de localitati pe baza abrevierii judetului
    $localitati = $u->CallMethod('Localities?countryId=1&countyId='.$judete[strtolower($judet['code'])], array(), 'GET', $token);

    echo '<option value="" rel="">-</option>'."\n";
    foreach ($localitati as $row) {
        echo '<option'.(trim(strtolower($val)) == trim(strtolower($row['Name'])) ? ' selected="selected"' : '').' km="'.($row['InNetwork'] ? 0 : $row['ExtraKm']).'">'.$row['Name'].'</option>'."\n";
    }

}
?>