﻿$(function () {
    var done = false;

    function do_replace() {

        console.log('UC. run replace');

        var element = $('[name="city"]');
        var type = element.prop('nodeName');

        var attr_name = element.attr('name');
        var attr_class = element.attr('class');
        var attr_id = element.attr('id');
        var maxlength = element.attr('maxlength');
        var data_validate = element.attr('data-validate');
        var value = element.val();

        if (element != null) {
            if ($('[name="id_country"]').val() == 36 && $('[name="id_state"]').val()) {
                $.post(urgentcargus_url + 'index.php?controller=urgentcargus&judet=' + $('[name="id_state"]').val() + '&val=' + value, function (data) {
                    element.replaceWith('<select style="width:100%" name="' + attr_name + '" class="' + attr_class + '" id="' + attr_id + '">' + data + '</select>');
                    console.log('UC. replace complete!');
                });
                console.log('UC. replace done');
                done = true;
            } else {
                if (type != 'INPUT') {
                    element.replaceWith('<input type="text" name="' + attr_name + '" class="' + attr_class + '" id="' + attr_id + '" maxlength="' + maxlength + '" data-validate="' + data_validate + '" value="" />');
                }
            }
        }
    }

    $(document).on('change', '[name="id_state"]', function () {
        console.log('UC. state changed');
        do_replace();
    });

    $(document).on('change', '[name="id_country"]', function () {
        console.log('UC. country changed');
        do_replace();
    });

    do_replace();

    $(document).ajaxComplete(function (event, request, settings) {
        if (!done) {
            if ($('[name="id_country"]').val() && $('[name="id_state"]').val()) {
                console.log('UC. ajax complete');
                do_replace();
                done = true;
            }
        }
    });
});